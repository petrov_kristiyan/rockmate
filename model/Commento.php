<?php

class Commento {
    private $id;
    private $idUpperType;
    private $madeby;
    private $testo;
    private $data;
    function __construct($id,$idUpperType, $madeby, $testo,$data) {
        $this->id = $id;
        $this->idUpperType = $idUpperType;
        $this->madeby = $madeby;
        $this->testo = $testo;
        $this->data = $data;
    }
    public function getId() {
        return $this->id;
    }
    
    public function setId($id) {
        $this->id = $id;
    }
    public function getTesto() {
        return $this->testo;
    }

    public function setTesto($testo) {
        $this->testo = $testo;
    }

    public function getIdUpperType() {
        return $this->idUpperType;
    }

    public function getMadeby() {
        return $this->madeby;
    }
    public function setIdUpperType($idUpperType) {
        $this->idUpperType = $idUpperType;
    }

    public function setMadeby($madeby) {
        $this->madeby = $madeby;
    }
    public function getData() {
        return $this->data;
    }

    public function setData($data) {
        $this->data = $data;
    }
}
?>