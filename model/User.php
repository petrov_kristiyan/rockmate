<?php

class User {

    private $id;
    private $nome;
    private $cognome;
    private $descrizione;
    private $dataN;
    private $luogoN;
    private $passwd;
    private $email;

    function __construct($id, $nome, $cognome, $descrizione,$dataN,$luogoN,$passwd,$email) {
        $this->id = $id;
        $this->nome = $nome;
        $this->cognome = $cognome;
        $this->descrizione = $descrizione;
        $this->dataN = $dataN;
        $this->luogoN = $luogoN;
        $this->passwd = $passwd;
        $this->email = $email;
    }

   
    public function getId() {
        return $this->id;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getCognome() {
        return $this->cognome;
    }

    public function getDescrizione() {
        return $this->descrizione;
    }

    public function getDataN() {
        return $this->dataN;
    }

    public function getLuogoN() {
        return $this->luogoN;
    }

    public function getPasswd() {
        return $this->passwd;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setCognome($cognome) {
        $this->cognome = $cognome;
    }

    public function setDescrizione($descrizione) {
        $this->descrizione = $descrizione;
    }

    public function setDataN($dataN) {
        $this->dataN = $dataN;
    }

    public function setLuogoN($luogoN) {
        $this->luogoN = $luogoN;
    }

    public function setPasswd($passwd) {
        $this->passwd = $passwd;
    }

    public function setEmail($email) {
        $this->email = $email;
    }
}
?>