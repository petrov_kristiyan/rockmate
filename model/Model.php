<?php

class Model {

//funzione usata per messaggi di tipo alert
    public function phpAlert($errorTitle, $msg) {
        if (!empty($_GET['show'])) {
            echo "<form method='post' action='index.php?show=" . $_GET['show'] . "' id='message'>
                <h3>" . $errorTitle . "</h3>
                <p>" . $msg . "</p>
                <input type='submit' value='OK'/>
             </form>";
        } else {
            echo "<form method='post' action='index.php' id='message'>
                <h3>" . $errorTitle . "</h3>
                <p>" . $msg . "</p>
                <input type='submit' value='OK'/>
             </form>";
        }
    }

    /*     * ********* bool registraUtente(email,password,nome,cognome); *********** */

    public function registraUtente($email, $passwd, $nome, $cognome) {
// Controllo sull email e password
        if ($this->validateInput($email) && $this->validateInput($passwd) && $this->validateInput($nome) && $this->validateInput($cognome)) {
            if (!empty($email) && !empty($passwd)) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
                    $this->phpAlert("Error", "Invalid email address. ");
                    return false;
                }
                $sql = "SELECT email FROM utenti WHERE email = '" . mysql_real_escape_string($email) . "' LIMIT 1";
                $query = mysql_query($sql) or trigger_error("Errore query: " . mysql_error());
                if (strlen($passwd) < 6) {
                    $this->phpAlert("Error", "The password must have more then 6 characters.");
                } else
                if (mysql_num_rows($query) == 1) {
                    $this->phpAlert("Error", "This email was already used.");
                } else {
// Errori non presenti e quindi inserisco nel database
                    $password = md5($passwd);
                    $sql = "INSERT INTO utenti ( `email`,`password`, `nome`, `cognome`) VALUES ('" . mysql_real_escape_string($email) . "','" . mysql_real_escape_string($password) . "','" . mysql_real_escape_string($nome) . "', '" . mysql_real_escape_string($cognome) . "');";
                    $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
                    $sql4 = "SELECT id FROM utenti WHERE email = '" . mysql_real_escape_string($email) . "' LIMIT 1";
                    $query4 = mysql_query($sql4) or trigger_error("Errore query: " . mysql_error());
                    $row4 = mysql_fetch_assoc($query4);
                    $id = $row4['id'];
                    $utente = new User($id, $nome, $cognome, "", "", "", "", $password, $email, null, null, null, null, null);
                    if ($query) {
                        $_SESSION['utente'] = $utente;
                        $_SESSION['loggedin'] = 1;
                        $fh = fopen("src/profilo.png", "r");
                        $image = addslashes(fread($fh, filesize("src/profilo.png")));
                        fclose($fh);
                        $_SESSION['utente']->setDataN(date('d M Y'));
                        $sql = "INSERT INTO informazioni ( `id`,`imgProfilo`,Data) VALUES ('" . mysql_real_escape_string($_SESSION['utente']->getId()) . "','" . $image . "',CURDATE());";
                        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
                        return true; //utente registrato
                    }
                }
            }
        }
        return false;
    }

    /*     * ********* bool loggedIn *********** */

//controlla la sessione e che l utente sia valido per essa
    public function loggedIn() {
        if (isset($_SESSION['loggedin']) && isset($_SESSION['utente'])) {
            return true;
        }
        return false;
    }

//funzione che ritorna tutti i Commenti degli stati più recenti dato il timestamp
    public function checkUpdateComments($stato, $timestamp) {
        $sql = "SELECT id,stato,madeby,testo,timestamp FROM commentostato WHERE timestamp >= from_unixtime('" . mysql_real_escape_string($timestamp) . "') and stato = '" . mysql_real_escape_string($stato) . "' order by timestamp DESC LIMIT 2";
        $query = mysql_query($sql) or trigger_error("Errore query: " . mysql_error());
        $array = array();
        if (mysql_num_rows($query) > 0) {
            while ($row = mysql_fetch_assoc($query)) {
                $array[] = new Commento($row['id'], $row['stato'], $row['madeby'], $row['testo'], $row['timestamp']);
            }
        }
        return $array;
    }

//funzione che inserisce una notifica a tutti quelli che hanno commentato e al proprietario dello stato.
    public function setNotificaStato($stato) {
        $owner = 0;
        //controlla che non sia l utente in sessione il proprietario dello stato, in tal caso aggiunge una notifica
        $sql2 = "SELECT userid FROM stato WHERE id=" . mysql_real_escape_string($stato) . " and userid!=" . mysql_real_escape_string($_SESSION['utente']->getId()) . "";
        $query2 = mysql_query($sql2) or trigger_error("Errore query: " . mysql_error());
        if (mysql_num_rows($query2) > 0) {
            $row2 = mysql_fetch_assoc($query2);
            $owner = $row2['userid'];
            $sql1 = "INSERT INTO notifichestato ( `userid`,`madeby`,`view`,`stato`) VALUES ('" . mysql_real_escape_string($row2['userid']) . "','" . mysql_real_escape_string($_SESSION['utente']->getId()) . "','1','" . mysql_real_escape_string($stato) . "');";
            $query1 = mysql_query($sql1) or trigger_error("Errore query: " . mysql_error());
        }
        //notifica tutti coloro che hanno commentato tranne l utente in sessione
        $sql = "SELECT distinct madeby FROM commentostato WHERE stato=" . mysql_real_escape_string($stato) . " and madeby!=" . mysql_real_escape_string($_SESSION['utente']->getId()) . " and madeby!=" . mysql_real_escape_string($owner) . "";
        $query = mysql_query($sql) or trigger_error("Errore query: " . mysql_error());
        if (mysql_num_rows($query) > 0) {
            while ($row = mysql_fetch_assoc($query)) {
                $sql1 = "INSERT INTO notifichestato ( `userid`,`madeby`,`view`,`stato`) VALUES ('" . mysql_real_escape_string($row['madeby']) . "','" . mysql_real_escape_string($_SESSION['utente']->getId()) . "','1','" . mysql_real_escape_string($stato) . "');";
                $query1 = mysql_query($sql1) or trigger_error("Errore query: " . mysql_error());
            }
        }
    }

//funzione che inserisce una notifica a tutti quelli che hanno commentato e al proprietario dello mappa.
    public function setNotificaMappaCommenti($img) {
        $owner = 0;
        //controlla che non sia l utente in sessione il proprietario dello stato, in tal caso aggiunge una notifica
        $sql2 = "SELECT userid FROM mappe WHERE id=" . mysql_real_escape_string($img) . " and userid!=" . mysql_real_escape_string($_SESSION['utente']->getId()) . "";
        $query2 = mysql_query($sql2) or trigger_error("Errore query: " . mysql_error());
        if (mysql_num_rows($query2) > 0) {
            $row2 = mysql_fetch_assoc($query2);
            $owner = $row2['userid'];
            $sql1 = "INSERT INTO notifichemappa ( `userid`,`madeby`,`view`,`type`,`img`) VALUES ('" . mysql_real_escape_string($row2['userid']) . "','" . mysql_real_escape_string($_SESSION['utente']->getId()) . "','1','0','" . mysql_real_escape_string($img) . "');";
            $query1 = mysql_query($sql1) or trigger_error("Errore query: " . mysql_error());
        }
        //notifica tutti coloro che hanno commentato tranne l utente in sessione
        $sql = "SELECT distinct madeby FROM commenti WHERE img=" . mysql_real_escape_string($img) . " and madeby!=" . mysql_real_escape_string($_SESSION['utente']->getId()) . " and madeby!=" . mysql_real_escape_string($owner) . "";
        $query = mysql_query($sql) or trigger_error("Errore query: " . mysql_error());
        if (mysql_num_rows($query) > 0) {
            while ($row = mysql_fetch_assoc($query)) {
                $sql1 = "INSERT INTO notifichemappa ( `userid`,`madeby`,`view`,`type`,`img`) VALUES ('" . mysql_real_escape_string($row['madeby']) . "','" . mysql_real_escape_string($_SESSION['utente']->getId()) . "','1','0','" . mysql_real_escape_string($img) . "');";
                $query1 = mysql_query($sql1) or trigger_error("Errore query: " . mysql_error());
            }
        }
    }

    //all aggiunta di una mappa notifica tutti gli amici dell utente
    public function setNotificaMappa($img) {
        $friends = $this->getFriends($_SESSION['utente']->getId());
        if (!empty($friends)) {
            foreach ($friends as $amico) {
                $sql1 = "INSERT INTO notifichemappa ( `userid`,`madeby`,`view`,`type`,`img`) VALUES ('" . mysql_real_escape_string($amico->getId()) . "','" . mysql_real_escape_string($_SESSION['utente']->getId()) . "','1','1','" . mysql_real_escape_string($img) . "');";
                $query1 = mysql_query($sql1) or trigger_error("Errore query: " . mysql_error());
            }
        }
    }

    //all aggiunta di una mappa notifica tutti gli amici dell utente
    public function setNotificaAmici($id) {
        $sql1 = "INSERT INTO notificheamici ( `toUser`,`fromUser`,`view`) VALUES ('" . mysql_real_escape_string($id) . "','" . mysql_real_escape_string($_SESSION['utente']->getId()) . "','1');";
        $query1 = mysql_query($sql1) or trigger_error("Errore query: " . mysql_error());
    }

    //follower non ancora aggiunti
    public function getNotificheAmici($id) {
        $sql = "SELECT id,toUser,fromUser,view  FROM notificheamici where toUser = '" . $id . "' and view=1 ";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $notifiche = array();
        if (mysql_num_rows($query) > 0) {
            while ($row = mysql_fetch_assoc($query)) {
                $notifiche[] = new Notifica($row['id'], $row['fromUser'], null, null);
            }
            return $notifiche;
        }
    }

    //ritorna le notifiche degli stati non ancora visualizzati
    public function getNotificheStato($id) {
        $sql = "SELECT id,stato,userid,madeby FROM notifichestato where userId = " . mysql_real_escape_string($id) . " and view='1'";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $notifiche = array();
        if (mysql_num_rows($query) > 0) {
            while ($row = mysql_fetch_assoc($query)) {
                $notifiche[] = new Notifica($row['id'], $row['madeby'], $row['stato'], null);
            }
            return $notifiche;
        }
    }

    //rimuove lo stato 
    public function removeStato($id, $uid) {
        $sql1 = "DELETE FROM stato WHERE userId = '" . mysql_real_escape_string($uid->getId()) . "'and id='" . mysql_real_escape_string($id) . "'";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
    }

    //rimuove il commento allo stato
    public function removeCommentoStato($id, $uid) {
        $sql1 = "DELETE FROM commentostato WHERE MadeBy = '" . mysql_real_escape_string($uid->getId()) . "'and id='" . mysql_real_escape_string($id) . "'";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
    }

    //elimina notifica relativa allo stato ( in realtà setta view a 0 , cioè come visualizzata )
    public function delNotificaStato($stato) {
        $sql = "Update notifichestato set view=0 where userId = " . mysql_real_escape_string($_SESSION['utente']->getId()) . " and stato =" . mysql_real_escape_string($stato) . " and view='1'";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $this->getNotificheStato($_SESSION['utente']->getId());
    }

    //elimina notifica relativa alla mappa( in realtà setta view a 0 , cioè come visualizzata)
    public function delNotificaMappa($mappa) {
        $sql = "Update notifichemappa set view=0 where userId = " . mysql_real_escape_string($_SESSION['utente']->getId()) . " and img =" . mysql_real_escape_string($mappa) . " and view='1'";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $this->getNotificheMappa($_SESSION['utente']->getId());
    }

    //elimina notifica relativa agli amici ( in realtà setta view a 0, cioè come visualizzata )
    public function delNotificaAmici($id) {
        $sql = "Update notificheamici set view=0 where toUser = " . mysql_real_escape_string($_SESSION['utente']->getId()) . " and id =" . mysql_real_escape_string($id) . " and view='1'";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $this->getNotificheAmici($_SESSION['utente']->getId());
    }

    //ritorna le notifiche relative alle mappe
    public function getNotificheMappa($id) {
        $sql = "SELECT id,img,userid,madeby,Type FROM notifichemappa where userId = " . mysql_real_escape_string($id) . " and view='1'";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $notifiche = array();
        if (mysql_num_rows($query) > 0) {
            while ($row = mysql_fetch_assoc($query)) {
                $notifiche[] = new Notifica($row['id'], $row['madeby'], $row['img'], $row['Type']);
            }
            return $notifiche;
        }
    }

    //ritorna l id del proprietario dello stato
    public function getStatoUserId($stato) {
        $sql = "SELECT userid FROM stato where id = " . mysql_real_escape_string($stato) . "";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $row = mysql_fetch_assoc($query);
        return $row['userid'];
    }

    //ritorna l id del proprietario della mappa
    public function getMappaUserId($mappa) {
        $sql = "SELECT userid FROM mappe where id = " . mysql_real_escape_string($mappa) . "";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $row = mysql_fetch_assoc($query);
        return $row['userid'];
    }

    //rimuove la mappa
    public function removeMappa($id, $uid) {
        $sql1 = "DELETE FROM mappe WHERE userId = '" . mysql_real_escape_string($uid->getId()) . "'and id='" . mysql_real_escape_string($id) . "'";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
    }

    //ritorna la mappa
    public function getMappa($mappa) {
        $sql = "SELECT id,userid,nome,descr,indirizzo,lat,lng,data FROM mappe where id = " . mysql_real_escape_string($mappa) . "";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $row = mysql_fetch_assoc($query);
        if (mysql_num_rows($query) > 0) {
            return new Mappa($row['id'], $row['userid'], $row['nome'], $row['descr'], $row['indirizzo'], $row['lat'], $row['lng'], $row['data'], $this->getCommentiMappa($row['id']));
        }
        return null;
    }

    /*     * *********bool logoutUser unset sulle variabili. *********** */

    public function logoutUser() {
// unset delle variabili di sessione
        unset($_SESSION['utente']);
        unset($_SESSION['loggedin']);
        unset($_SESSION['amico']);
        session_destroy();
        return true;
    }

    /*     * ********* bool verifica login Utente *********** */

    public function validaUtente($email, $passwd) {
        $password = md5($passwd);
        if ($this->validateInput($passwd) && $this->validateInput($email)) {
            $sql = "SELECT id,nome,cognome,email,password FROM utenti WHERE email = '" . mysql_real_escape_string($email) . "' AND password = '" . mysql_real_escape_string($password) . "'";
            $query = mysql_query($sql) or trigger_error("Query Failed: " . mysql_error());
// If one row was returned, the user was logged in!
            if (mysql_num_rows($query) == 1) {
                $row = mysql_fetch_assoc($query);
                $utente = new User($row['id'], $row['nome'], $row['cognome'], $this->getAbout($row['id']), $this->getNascita($row['id']), $this->getLuogo($row['id']), $row['password'], $row['email']);
                $_SESSION['utente'] = $utente;
                if ($this->validaAdmin($row['id'])) {
                    $_SESSION['loggedin'] = 2;
                    return 2; // è un admin
                } else {
                    $_SESSION['loggedin'] = 1;
                    return 1; // è un utente
                }
            }
        }
        return 0; //non esiste o dati non corretti
    }

    //controlla se è un amministratore
    public function validaAdmin($id) {
        $sql = "SELECT id FROM admin WHERE id='" . mysql_real_escape_string($id) . "'";
        $query = mysql_query($sql) or trigger_error("Query Failed: " . mysql_error());
        if (mysql_num_rows($query) == 1)
            return true;
        else
            return false;
    }

    //ritorna l email del utente
    public function getEmail($id) {
        $sql = "SELECT email FROM utenti WHERE id = '" . mysql_real_escape_string($id) . "' LIMIT 1";
        $query = mysql_query($sql) or trigger_error("Errore query: " . mysql_error());
        $row = mysql_fetch_assoc($query);
        return $row['email'];
    }

    //set email
    public function setEmail($uid, $email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
            $this->phpAlert("Error", "Invalid email address");
            return false;
        }
        if ($this->validateInput($email)) {
            $uid->setEmail($email);
            $sql1 = "SELECT * FROM utenti WHERE id='" . mysql_real_escape_string($uid->getId()) . "' ";
            $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
            if (mysql_num_rows($query1) > 0) {
                $sql2 = "UPDATE utenti SET email = '" . mysql_real_escape_string($email) . "' WHERE id='" . mysql_real_escape_string($uid->getId()) . "' ";
                $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
            }
            return true;
        }
    }

//get Nome Utente passato come parametro
    public function getNome($id) {
        $sql = "SELECT nome FROM utenti WHERE id = '" . mysql_real_escape_string($id) . "' LIMIT 1";
        $query = mysql_query($sql) or trigger_error("Errore query: " . mysql_error());
        $row = mysql_fetch_assoc($query);
        return $row['nome'];
    }

    //set nome mappa
    public function setNomeM($mappa, $nome) {
        if ($this->validateInput($nome)) {
            if ($nome != '' && preg_match("/^(\s)+$/", $nome) === 0) {
                $sql2 = "UPDATE mappe SET nome = '" . mysql_real_escape_string($nome) . "' WHERE id='" . mysql_real_escape_string($mappa->getId()) . "' ";
                $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
            }
        }
    }

    //set descrizione mappa
    public function setDescrM($mappa, $descr) {
        if ($this->validateInput($descr)) {
            if ($descr != '' && preg_match("/^(\s)+$/", $descr) === 0) {
                $sql2 = "UPDATE mappe SET descr = '" . mysql_real_escape_string($descr) . "' WHERE id='" . mysql_real_escape_string($mappa->getId()) . "' ";
                $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
            }
        }
    }

    //rimuove commento della mappa
    public function removeComments($mappa) {
        $sql = "DELETE FROM commenti WHERE img = '" . mysql_real_escape_string($mappa->getId()) . "'";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        if ($query)
            return true;
        else {
            return false;
        }
    }

    //set nome del utente
    public function setNome($uid, $nome) {
        if ($this->validateInput($nome)) {
            $uid->setNome($nome);
            if (preg_match("/^(\s)+$/", $nome) === 0) {
                $sql1 = "SELECT * FROM utenti WHERE id='" . mysql_real_escape_string($uid->getId()) . "' ";
                $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
                if (mysql_num_rows($query1) > 0) {
                    $sql2 = "UPDATE utenti SET nome = '" . mysql_real_escape_string($nome) . "' WHERE id='" . mysql_real_escape_string($uid->getId()) . "' ";
                    $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
                }
            }
            return true;
        }
    }

//get Cognome del Utente passato come parametro
    public function getCognome($id) {
        $sql = "SELECT cognome FROM utenti WHERE id = '" . mysql_real_escape_string($id) . "' LIMIT 1";
        $query = mysql_query($sql) or trigger_error("Errore query: " . mysql_error());
        $row = mysql_fetch_assoc($query);
        return $row['cognome'];
    }

    //set cognome utente
    public function setCognome($uid, $cognome) {
        if ($this->validateInput($cognome)) {
            $uid->setCognome($cognome);
            if ($cognome != '' && preg_match("/^(\s)+$/", $cognome) === 0) {
                $sql1 = "SELECT * FROM utenti WHERE id='" . mysql_real_escape_string($uid->getId()) . "' ";
                $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
                if (mysql_num_rows($query1) > 0) {
                    $sql2 = "UPDATE utenti SET cognome = '" . mysql_real_escape_string($cognome) . "' WHERE id='" . mysql_real_escape_string($uid->getId()) . "' ";
                    $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
                }
            }
            return true;
        }
    }

//get Nome Utente passato come parametro
    public function setPasswd($passwd1, $passwd2) {
        if ($this->validateInput($passwd1) && $this->validateInput($passwd2)) {
            if (preg_match("/^(\s)+$/", $passwd1) === 0) {
                $password = md5($passwd1);
                $sql = "SELECT password FROM utenti WHERE email = '" . mysql_real_escape_string($_SESSION['utente']->getEmail()) . "' AND password = '" . mysql_real_escape_string($password) . "'";
                $query = mysql_query($sql) or trigger_error("Query Failed: " . mysql_error());
                if (mysql_num_rows($query) == 1) {
                    $password2 = md5($passwd2);
                    $sql2 = "UPDATE utenti SET password = '" . mysql_real_escape_string($password2) . "' WHERE id='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' ";
                    $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
                    $_SESSION['utente']->setPasswd($password2);
                    return true;
                }
                $this->phpAlert("Error", "Submited wrong password");
            } else
                $this->phpAlert("Error", "Please fill all data fields.");
        }
    }

//metodo per l amministratore per cambiare la password all utente senza conoscere quella vecchia
    public function setPasswdAdmin($passwd2, $uid) {
        if ($this->validateInput($passwd2)) {
            if ($passwd2 != '' && preg_match("/^(\s)+$/", $passwd2) === 0) {
                $password2 = md5($passwd2);
                $sql2 = "UPDATE utenti SET password = '" . mysql_real_escape_string($password2) . "' WHERE id='" . mysql_real_escape_string($uid->getId()) . "' ";
                $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
                if ($query2)
                    return true;
            }
        }
        return false;
    }

    /*     * *********************** get Data di Nascita ******************************* */

    public function getNascita($id) {
        $sql1 = "SELECT data FROM informazioni WHERE id ='" . mysql_real_escape_string($id) . "' ";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
        $row = mysql_fetch_assoc($query1);
        if ($row['data'] != '')
            return date('d M Y', strtotime($row['data']));
        else
            return date('d M Y');
    }

    /*     * *********************** get About del profilo ******************************* */

    public function getAbout($id) {
        $sql1 = "SELECT about FROM informazioni WHERE id='" . mysql_real_escape_string($id) . "' ";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
        $row = mysql_fetch_assoc($query1);
        return $row['about'];
    }

    /*     * *********************** set About del profilo ******************************* */

    public function setAbout($uid, $about) {
        if ($this->validateInput($about)) {
            $uid->setDescrizione($about);
            if ($about != '' && preg_match("/^(\s)+$/", $about) === 0) {
                $sql2 = "UPDATE informazioni SET about = '" . mysql_real_escape_string($about) . "' WHERE id='" . mysql_real_escape_string($uid->getId()) . "' ";
                $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
            }
        }
    }

    /*     * *********************** Get Luogo di nascita ******************************* */

    public function getLuogo($id) {
        $sql1 = "SELECT luogo FROM informazioni WHERE id='" . mysql_real_escape_string($id) . "' ";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
        $row = mysql_fetch_assoc($query1);
        return $row['luogo'];
    }

    /*     * *********************** Set Luogo di nascita ******************************* */

    public function setLuogo($uid, $luogo) {
        if ($this->validateInput($luogo)) {
            $uid->setLuogoN($luogo);
            if ($luogo != '' && preg_match("/^(\s)+$/", $luogo) === 0) {
                $sql2 = "UPDATE informazioni SET luogo = '" . mysql_real_escape_string($luogo) . "' WHERE id='" . mysql_real_escape_string($uid->getId()) . "' ";
                $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
            }
        }
    }

    //ordina i risultati della ricerca 
    public function ordina($type, $fullNome) {
        //nome decrescente
        if ($type == 1) {
            if ($this->validateInput($fullNome)) {
                $friend = array();
                $sql2 = "SELECT id FROM utenti WHERE ( CONCAT_WS(' ', Nome, Cognome)  LIKE '%" . mysql_real_escape_string($fullNome) . "%') and id != '" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' and id NOT IN (SELECT id1 from amici where ID2='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' UNION SELECT id2 from amici where ID1='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "') ORDER by Nome DESC";
                $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
                if (mysql_num_rows($query2) > 0) {
                    while ($row = mysql_fetch_assoc($query2)) {
                        $friend[] = new User($row["id"], $this->getNome($row["id"]), $this->getCognome($row["id"]), $this->getAbout($row["id"]), $this->getNascita($row["id"]), $this->getLuogo($row["id"]), null, $this->getEmail($row["id"]));
                    }
                    return $friend;
                }
                return null;
            }
        }
        //nome ascendente 
        if ($type == 2) {
            if ($this->validateInput($fullNome)) {
                $friend = array();
                $sql2 = "SELECT id FROM utenti WHERE ( CONCAT_WS(' ', Nome, Cognome)  LIKE '%" . mysql_real_escape_string($fullNome) . "%') and id != '" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' and id NOT IN (SELECT id1 from amici where ID2='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' UNION SELECT id2 from amici where ID1='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "') ORDER by Nome ASC";
                $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
                if (mysql_num_rows($query2) > 0) {
                    while ($row = mysql_fetch_assoc($query2)) {
                        $friend[] = new User($row["id"], $this->getNome($row["id"]), $this->getCognome($row["id"]), $this->getAbout($row["id"]), $this->getNascita($row["id"]), $this->getLuogo($row["id"]), null, $this->getEmail($row["id"]));
                    }
                    return $friend;
                }
                return null;
            }
        }
        //luogo ascendente
        if ($type == 3) {
            if ($this->validateInput($fullNome)) {
                $friend = array();
                $sql2 = "SELECT utenti.id FROM informazioni, utenti WHERE ( CONCAT_WS(' ', Nome, Cognome)  LIKE '%" . mysql_real_escape_string($fullNome) . "%') and utenti.id=informazioni.id and utenti.id != '" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' and utenti.id NOT IN (SELECT id1 from amici where ID2='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' UNION SELECT id2 from amici where ID1='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "') ORDER by Luogo ASC";
                $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
                if (mysql_num_rows($query2) > 0) {
                    while ($row = mysql_fetch_assoc($query2)) {
                        $friend[] = new User($row["id"], $this->getNome($row["id"]), $this->getCognome($row["id"]), $this->getAbout($row["id"]), $this->getNascita($row["id"]), $this->getLuogo($row["id"]), null, $this->getEmail($row["id"]));
                    }
                    return $friend;
                }
                return null;
            }
        }
        //luogo decrescente
        if ($type == 4) {
            if ($this->validateInput($fullNome)) {
                $friend = array();
                $sql2 = "SELECT utenti.id FROM informazioni,utenti WHERE ( CONCAT_WS(' ', Nome, Cognome)  LIKE '%" . mysql_real_escape_string($fullNome) . "%') and utenti.id=informazioni.id and utenti.id != '" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' and utenti.id NOT IN (SELECT id1 from amici where ID2='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' UNION SELECT id2 from amici where ID1='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "') ORDER by Luogo DESC";
                $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
                if (mysql_num_rows($query2) > 0) {
                    while ($row = mysql_fetch_assoc($query2)) {
                        $friend[] = new User($row["id"], $this->getNome($row["id"]), $this->getCognome($row["id"]), $this->getAbout($row["id"]), $this->getNascita($row["id"]), $this->getLuogo($row["id"]), null, $this->getEmail($row["id"]));
                    }
                    return $friend;
                }
                return null;
            }
        }
        //data ascendente
        if ($type == 5) {
            if ($this->validateInput($fullNome)) {
                $friend = array();
                $sql2 = "SELECT utenti.id FROM informazioni,utenti WHERE ( CONCAT_WS(' ', Nome, Cognome)  LIKE '%" . mysql_real_escape_string($fullNome) . "%') and utenti.id=informazioni.id and utenti.id != '" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' and utenti.id NOT IN (SELECT id1 from amici where ID2='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' UNION SELECT id2 from amici where ID1='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "') ORDER by Data ASC";
                $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
                if (mysql_num_rows($query2) > 0) {
                    while ($row = mysql_fetch_assoc($query2)) {
                        $friend[] = new User($row["id"], $this->getNome($row["id"]), $this->getCognome($row["id"]), $this->getAbout($row["id"]), $this->getNascita($row["id"]), $this->getLuogo($row["id"]), null, $this->getEmail($row["id"]));
                    }
                    return $friend;
                }
                return null;
            }
        }
        //data decrescente
        if ($type == 6) {
            if ($this->validateInput($fullNome)) {
                $friend = array();
                $sql2 = "SELECT utenti.id FROM informazioni,utenti WHERE ( CONCAT_WS(' ', Nome, Cognome)  LIKE '%" . mysql_real_escape_string($fullNome) . "%') and utenti.id=informazioni.id and utenti.id != '" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' and utenti.id NOT IN (SELECT id1 from amici where ID2='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' UNION SELECT id2 from amici where ID1='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "') ORDER by Data DESC";
                $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
                if (mysql_num_rows($query2) > 0) {
                    while ($row = mysql_fetch_assoc($query2)) {
                        $friend[] = new User($row["id"], $this->getNome($row["id"]), $this->getCognome($row["id"]), $this->getAbout($row["id"]), $this->getNascita($row["id"]), $this->getLuogo($row["id"]), null, $this->getEmail($row["id"]));
                    }
                    return $friend;
                }
                return null;
            }
        }
    }

    /*     * *********************** set Data di Nascita  ******************************* */

    public function setNascita($uid, $d, $m, $y) {
        $data = DateTime::createFromFormat('m-d-Y', $m . "-" . $d . "-" . $y)->format('Y-m-d');
        $uid->setDataN(DateTime::createFromFormat('m-d-Y', $m . "-" . $d . "-" . $y)->format('d M Y'));
        $sql2 = "UPDATE informazioni SET data = '" . mysql_real_escape_string($data) . "' WHERE id='" . mysql_real_escape_string($uid->getId()) . "' ";
        $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
    }

    /*     * *********************** get Messaggio di stato ******************************* */

    public function getStati($id) {
        $sql1 = "SELECT id,userid,testo,data FROM stato WHERE userid ='" . mysql_real_escape_string($id) . "' order by data DESC ";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
        $array = array();
        if (mysql_num_rows($query1) > 0) {
            while ($row = mysql_fetch_assoc($query1)) {
                $array[] = new Stato($row['id'], $row['userid'], $row['testo'], $row['data'], $this->getCommentiStati($row['id'], $id));
            }
        }
        return $array;
    }

    //ritorna gli stati del utente in sessione e dei suoi amici
    public function getStatiHome($id) {
        $sql = "SELECT id,userid,testo,data FROM stato, amici WHERE userid = id2 and ID1='" . mysql_real_escape_string($id) . "' and id2 in( SELECT id1 from amici where ID2='" . mysql_real_escape_string($id) . "') UNION SELECT id,userid,testo,data from stato where userid ='" . mysql_real_escape_string($id) . "' order by data DESC ";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $array = array();
        if (mysql_num_rows($query) > 0) {
            while ($row = mysql_fetch_assoc($query)) {
                $array[] = new Stato($row['id'], $row['userid'], $row['testo'], $row['data'], $this->getCommentiStati($row['id'], $_SESSION['utente']->getId()));
            }
        }
        return $array;
    }

    //ritorna i stati più nuovi dato il timestamp
    public function checkUpdateStati($timestamp) {
        $sql1 = "SELECT id,userid,testo,data FROM stato,amici WHERE data > from_unixtime('" . mysql_real_escape_string($timestamp) . "') and userid = id2 and ID1='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' and id2 in( SELECT id1 from amici where ID2='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "') UNION SELECT id,userid,testo,data from stato where userid ='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' and data > from_unixtime('" . mysql_real_escape_string($timestamp) . "') order by data DESC ";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
        $array = array();
        if (mysql_num_rows($query1) > 0) {
            while ($row = mysql_fetch_assoc($query1)) {
                $array[] = new Stato($row['id'], $row['userid'], $row['testo'], $row['data'], $this->getCommentiStati($row['id']));
            }
        }
        return $array;
    }

    //ritorna le mappe più nuove dato il timestamp
    public function checkUpdateMappe($timestamp) {
        $sql1 = "SELECT id,userid,nome,descr,indirizzo,lat,lng,data FROM mappe,amici WHERE data > from_unixtime('" . mysql_real_escape_string($timestamp) . "') and userid = id2 and ID1='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' and id2 in( SELECT id1 from amici where ID2='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "') UNION SELECT id,userid,nome,descr,indirizzo,lat,lng,data FROM mappe where userid ='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' and data > from_unixtime('" . mysql_real_escape_string($timestamp) . "') order by data DESC ";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
        $array = array();
        if (mysql_num_rows($query1) > 0) {
            while ($row = mysql_fetch_assoc($query1)) {
                $array[] = new Mappa($row['id'], $row['userid'], $row['nome'], $row['descr'], $row['indirizzo'], $row['lat'], $row['lng'], $row['data'], $this->getCommentiMappa($row['id']));
            }
        }
        return $array;
    }

    /*     * ***********************get Commenti Stati ******************************* */

    public function getCommentiStati($stato) {
        $sql = "SELECT id,stato,madeby,testo,timestamp FROM commentostato WHERE stato='" . mysql_real_escape_string($stato) . "'order by timestamp DESC";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $array = array();
        if (mysql_num_rows($query) > 0) {
            while ($row = mysql_fetch_assoc($query)) {
                $array[] = new Commento($row['id'], $row['stato'], $row['madeby'], $row['testo'], $row['timestamp']);
            }
        }
        return $array;
    }

    /*     * *********************** Set messaggio di stato******************************* */

    public function setStatus($stato) {
        $status = array();
        if (preg_match("/^(\s)+$/", $stato) === 0) {
            if ($this->validateInput($stato)) {
                $id = mysql_result(mysql_query("SELECT MAX(id) FROM stato"), 0);
                $id++;
                $sql = "INSERT INTO stato (`id`, `testo`,`userId`,`data`) VALUES ('" . mysql_real_escape_string($id) . "','" . mysql_real_escape_string($stato) . "','" . mysql_real_escape_string($_SESSION['utente']->getId()) . "',current_timestamp());";
                $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
                $status = new Stato($id, $_SESSION['utente']->getId(), $stato, date("Y-m-d H:i:s"), null);
            }
        }
        return $status;
    }

    //controlla input
    public function validateInput($msg) {
        if (preg_match("/\+|\||\-\-|\=|\<|\>|\!\=|\(|\)|\%|\*/", $msg) === 1) {
            $this->phpAlert("Error", "You have inserted one or more of the following characters : |    +    --    =    <   >   !=   (   )  %   * ");
            return false;
        } else
            return true;
    }

    /*     * *********************** get Img del Profilo ******************************* */

    public function getImgProfilo($id) {
        $sql1 = "SELECT imgProfilo FROM informazioni WHERE id ='" . $id . "' ";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
        $row = mysql_fetch_assoc($query1);
        return "data:image/png;base64," . base64_encode($row['imgProfilo']);
    }

    /*     * *********************** set Immagine di Profilo ******************************* */

    public function setImgProfilo() {
        $tmp_nome = $_FILES["profilo"]["tmp_name"];
        $tipo = $_FILES["profilo"]["type"];
        if ($tipo != '')
            if (!$this->controllaTipo($tipo))
                $this->phpAlert("Error","Unknown file type");
            else {
                $image = addslashes(file_get_contents($_FILES["profilo"]['tmp_name']));
                $sql2 = "UPDATE informazioni SET imgProfilo = '" . $image . "' WHERE id='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' ";
                $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
            }
    }

    /*     * *********************** mostra Utente da aggiungere agli amici tramite il Nome ******************************* */

    public function showF($fullNome) {
        if ($this->validateInput($fullNome)) {
            $friend = array();
            $sql2 = "SELECT id FROM utenti WHERE ( CONCAT_WS(' ', Nome, Cognome)  LIKE '%" . mysql_real_escape_string($fullNome) . "%') and id != '" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' and id NOT IN (SELECT id1 from amici where ID2='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' UNION SELECT id2 from amici where ID1='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "')";
            $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
            if (mysql_num_rows($query2) > 0) {
                while ($row = mysql_fetch_assoc($query2)) {
                    $friend[] = new User($row["id"], $this->getNome($row["id"]), $this->getCognome($row["id"]), $this->getAbout($row["id"]), $this->getNascita($row["id"]), $this->getLuogo($row["id"]), null, $this->getEmail($row["id"]));
                }
                return $friend;
            }
            return null;
        }
    }

    //funzione per la search bar
    public function searchNome($data) {
        $user = array();
        //utenti con nome o cognome =  a $data 
        $sql1 = "SELECT id FROM utenti WHERE ( CONCAT_WS(' ', Nome, Cognome)  LIKE '%" . mysql_real_escape_string($data) . "%') and id != '" . mysql_real_escape_string($_SESSION['utente']->getId()) . "'";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
        if (mysql_num_rows($query1) > 0) {
            while ($row = mysql_fetch_assoc($query1)) {
                $user[] = new User($row["id"], $this->getNome($row["id"]), $this->getCognome($row["id"]), $this->getAbout($row["id"]), $this->getNascita($row["id"]), $this->getLuogo($row["id"]), null, $this->getEmail($row["id"]));
            }
        }
        return $user;
    }

    public function searchLuogoNascita($data) {
        $user2 = array();
        //utenti con luogo di nascita =  a $data
        $sql2 = "SELECT id FROM informazioni WHERE ( luogo  LIKE '%" . mysql_real_escape_string($data) . "%' and id != '" . mysql_real_escape_string($_SESSION['utente']->getId()) . "')";
        $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
        if (mysql_num_rows($query2) > 0) {
            while ($row = mysql_fetch_assoc($query2)) {
                $user2[] = new User($row["id"], $this->getNome($row["id"]), $this->getCognome($row["id"]), $this->getAbout($row["id"]), $this->getNascita($row["id"]), $this->getLuogo($row["id"]), null, $this->getEmail($row["id"]));
            }
        }
        return $user2;
    }

    public function searchLuogoMappe($data) {
        //mappe degli amici con luogo = a $data
        $sql3 = "SELECT id,nome,userId,descr,indirizzo,lat,lng,data FROM mappe WHERE Indirizzo LIKE '%" . mysql_real_escape_string($data) . "%' and userid IN (SELECT id2 from amici where ID1='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' and id2 in( SELECT id1 from amici where ID2='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "'))";
        $query3 = mysql_query($sql3) or trigger_error("Errore Query: " . mysql_error());
        $mappe = array();
        if (mysql_num_rows($query3) > 0) {
            while ($row = mysql_fetch_assoc($query3)) {
                $mappe[] = new Mappa($row['id'], $row['userId'], $row['nome'], $row['descr'], $row['indirizzo'], $row['lat'], $row['lng'], $row['data'], $this->getCommentiMappa($row['id'], $row['userId']));
            }
        }
        return $mappe;
    }

    public function searchLuogoMappeNA($data) {
        //utenti non amici aventi mappe con luogo = a $data 
        $sql4 = "SELECT distinct userid FROM mappe WHERE Indirizzo LIKE '%" . mysql_real_escape_string($data) . "%' and userid NOT IN (SELECT id2 from amici where ID1='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "' and id2 in( SELECT id1 from amici where ID2='" . mysql_real_escape_string($_SESSION['utente']->getId()) . "'))";
        $query4 = mysql_query($sql4) or trigger_error("Errore Query: " . mysql_error());
        $user3 = array();
        if (mysql_num_rows($query4) > 0) {
            while ($row = mysql_fetch_assoc($query4)) {
                $user3[] = new User($row["userid"], $this->getNome($row["userid"]), $this->getCognome($row["userid"]), $this->getAbout($row["userid"]), $this->getNascita($row["userid"]), $this->getLuogo($row["userid"]), null, $this->getEmail($row["userid"]));
            }
        }
        return $user3;
    }

    //ritorna tutti gli utenti (funzione per l amministratore)
    public function getUtenti() {
        $sql = "SELECT id from utenti";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $array = array();
        if (mysql_num_rows($query) > 0) {
            while ($row = mysql_fetch_assoc($query)) {
                $array[] = new User($row["id"], $this->getNome($row["id"]), $this->getCognome($row["id"]), $this->getAbout($row["id"]), $this->getNascita($row["id"]), $this->getLuogo($row["id"]), null, $this->getEmail($row["id"]));
            }
        }
        return $array;
    }

    // ritorna l utente dato l id
    public function getUtente($id) {
        $lista = $this->getUtenti();
        foreach ($lista as $tmp) {
            if ($tmp->getId() === $id) {
                return $tmp;
            }
        }
    }

    //rimuove l utente ( amministratore )
    public function removeUser($uid) {
        $sql = "DELETE FROM utenti WHERE id = '" . mysql_real_escape_string($uid->getId()) . "'";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        if ($query)
            return true;
        else {
            return false;
        }
    }

    //rimuove tutte le mappe( amministratore )
    public function removeMaps($uid) {
        $sql = "DELETE FROM mappe WHERE userId = '" . mysql_real_escape_string($uid->getId()) . "'";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        if ($query)
            return true;
        else {
            return false;
        }
    }

    /*     * *********************** Get lista di Amici ******************************* */

    public function getFriends($id) {
        $sql = "SELECT id2 from amici where ID1='" . mysql_real_escape_string($id) . "' and id2 in( SELECT id1 from amici where ID2='" . mysql_real_escape_string($id) . "')";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $array = array();
        if (mysql_num_rows($query) > 0) {
            while ($row = mysql_fetch_assoc($query)) {
                $array[] = new User($row["id2"], $this->getNome($row["id2"]), $this->getCognome($row["id2"]), $this->getAbout($row["id2"]), $this->getNascita($row["id2"]), $this->getLuogo($row["id2"]), null, $this->getEmail($row["id2"]));
            }
        }
        return $array;
    }

    // lista dei followers(cioè coloro che non ho ancora accettato come amici )
    public function getFollowers($id) {
        $sql = "SELECT id1  from amici where id2 =" . mysql_real_escape_string($id) . " and id1 not in (SELECT id2 from amici where ID1='" . mysql_real_escape_string($id) . "' and id2 in( SELECT id1 from amici where ID2='" . mysql_real_escape_string($id) . "'))";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $array = array();
        if (mysql_num_rows($query) > 0) {
            while ($row = mysql_fetch_assoc($query)) {
                $array[] = new User($row["id1"], $this->getNome($row["id1"]), $this->getCognome($row["id1"]), $this->getAbout($row["id1"]), $this->getNascita($row["id1"]), $this->getLuogo($row["id1"]), null, $this->getEmail($row["id1"]));
            }
        }
        return $array;
    }

    // lista dei following ( cioè coloro che non mi hanno ancora accettato come amico )
    public function getFollowing($id) {
        $sql = "SELECT id2  from amici where id1 = " . mysql_real_escape_string($id) . " and id2 not in (SELECT id2 from amici where ID1='" . mysql_real_escape_string($id) . "' and id2 in( SELECT id1 from amici where ID2='" . mysql_real_escape_string($id) . "'))";
        $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
        $array = array();
        if (mysql_num_rows($query) > 0) {
            while ($row = mysql_fetch_assoc($query)) {
                $array[] = new User($row["id2"], $this->getNome($row["id2"]), $this->getCognome($row["id2"]), $this->getAbout($row["id2"]), $this->getNascita($row["id2"]), $this->getLuogo($row["id2"]), null, $this->getEmail($row["id2"]));
            }
        }
        return $array;
    }

    /*     * *********************** Aggiunge Utente agli amici ******************************* */

    public function addF($id) {
        if ($id != '') {
            $sql1 = "SELECT * from utenti where id =" . mysql_real_escape_string($id) . " and id IN (SELECT id2 from amici where ID1='" . mysql_real_escape_string($id) . "' and id2 in( SELECT id1 from amici where ID2='" . mysql_real_escape_string($id) . "'))";
            $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
            if (mysql_num_rows($query1) === 0) {
                $sql = "INSERT INTO amici ( `id1`,`id2`) VALUES ('" . mysql_real_escape_string(mysql_real_escape_string($_SESSION['utente']->getId())) . "','" . mysql_real_escape_string($id) . "')";
                $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
                $this->setNotificaAmici($id);
                return new User($id, $this->getNome($id), $this->getCognome($id), $this->getAbout($id), $this->getNascita($id), $this->getLuogo($id), null, $this->getEmail($id));
            } else
                $this->phpAlert("Error", "The user is already your friend");
        }
    }

    //ritorna le mappe del utente in sessione
    public function getMappe($id) {
        $sql1 = "SELECT id,nome,userId,descr,indirizzo,lat,lng,data from mappe where userId=" . mysql_real_escape_string($id) . "";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
        $array = array();
        if (mysql_num_rows($query1) > 0) {
            while ($row = mysql_fetch_assoc($query1)) {
                $array[] = new Mappa($row['id'], $row['userId'], $row['nome'], $row['descr'], $row['indirizzo'], $row['lat'], $row['lng'], $row['data'], $this->getCommentiMappa($row['id'], $row['userId']));
            }
        }
        return $array;
    }

    //ritorna le mappe del utente in sessione e dei suoi amici
    public function getMappeHome($id) {
        $sql1 = "SELECT id,userId,nome,descr,indirizzo,lat,lng,data FROM mappe, amici WHERE userId = id2 and ID1='" . mysql_real_escape_string($id) . "' and id2 in( SELECT id1 from amici where ID2='" . mysql_real_escape_string($id) . "') UNION SELECT id,userId,nome,descr,indirizzo,lat,lng,data FROM mappe where userId ='" . mysql_real_escape_string($id) . "' order by data DESC ";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
        $array = array();
        if (mysql_num_rows($query1) > 0) {
            while ($row = mysql_fetch_assoc($query1)) {
                $array[] = new Mappa($row['id'], $row['userId'], $row['nome'], $row['descr'], $row['indirizzo'], $row['lat'], $row['lng'], $row['data'], $this->getCommentiMappa($row['id'], $row['userId']));
            }
        }
        return $array;
    }

    /*     * ***********************get Commenti della Mappa ******************************* */

    public function getCommentiMappa($mappa) {
        $sql1 = "SELECT id,img,madeby,testo,timestamp FROM commenti WHERE img='" . mysql_real_escape_string($mappa) . "'order by timestamp DESC";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
        $array = array();
        if (mysql_num_rows($query1) > 0) {
            while ($row = mysql_fetch_assoc($query1)) {
                $array[] = new Commento($row['id'], $row['img'], $row['madeby'], $row['testo'], $row['timestamp']);
            }
        }
        return $array;
    }

    /*     * *********************** Aggiunge Commenti alla mappa ******************************* */

    public function addCommentsMappa($img, $testo) {
        if ($this->validateInput($testo)) {
            $sql = "SELECT id FROM mappe WHERE id='" . mysql_real_escape_string($img) . "'";
            $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
            if (mysql_num_rows($query) > 0) {
                $sql1 = "INSERT INTO commenti ( `img`,`testo`,`madeby`) VALUES ('" . mysql_real_escape_string($img) . "','" . mysql_real_escape_string($testo) . "','" . mysql_real_escape_string($_SESSION['utente']->getId()) . "');";
                $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
                $this->setNotificaMappaCommenti($img);
                return true;
            } else {
                return false;
            }
        }
    }

    //visualizzazione della data in tempo passato
    public function showTimestamp($data1) {
        $data2 = date_create();
        $diffY = intval($data2->format("Y")) - intval($data1->format("Y"));
        $diffM = intval($data2->format("m")) - intval($data1->format("m"));
        $diffD = intval($data2->format("d")) - intval($data1->format("d"));
        $diffH = intval($data2->format("h")) - intval($data1->format("h"));
        $diffI = intval($data2->format("i")) - intval($data1->format("i"));
        if ($diffY == 0) {
            if ($diffM == 0) {
                if ($diffD == 0) {
                    if ($diffH == 0) {
                        if ($diffI == 1)
                            return "" . $diffI . " minute ago";
                        else
                            return "" . $diffI . " minutes ago";
                    } else {
                        if ($diffD)
                            return "Yesterday at" . ($data1->format(" H:")) . ($data1->format("i"));
                        else
                            return "Today at" . ($data1->format(" H:")) . ($data1->format("i"));
                    }
                }else {
                    if ($diffD == 1) {
                        return "Yesterday at" . ($data1->format(" H:")) . ($data1->format("i"));
                    } else
                        return $data1->format("D") . " at" . ($data1->format(" H:")) . ($data1->format("i"));
                }
            } else
                return "on " . $data1->format(" d ") . ($data1->format(" F ")) . "at" . ($data1->format(" H:")) . ($data1->format("i"));
        } else
            return " in " . $data1->format(" Y ") . " on " . $data1->format(" d ") . ($data1->format(" F ")) . "at" . ($data1->format(" H:")) . ($data1->format("i"));
    }

    //aggiungi commento allo stato
    public function addCommentsStato($id, $testo) {
        if ($this->validateInput($testo)) {
            $sql1 = "SELECT id FROM stato WHERE id='" . mysql_real_escape_string($id) . "'";
            $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
            if (mysql_num_rows($query1) > 0) {
                $sql2 = "INSERT INTO commentostato ( `stato`,`testo`,`madeby`) VALUES ('" . mysql_real_escape_string($id) . "','" . mysql_real_escape_string($testo) . "','" . mysql_real_escape_string($_SESSION['utente']->getId()) . "');";
                $query2 = mysql_query($sql2) or trigger_error("Errore Query: " . mysql_error());
                $this->setNotificaStato($id);
                return true;
            } else {
                return false;
            }
        }
    }

    /*     * *********************** get Immagine thumb******************************* */

    public function getMapThumb($indirizzo, $lat, $lng) {
        return "http://maps.googleapis.com/maps/api/staticmap?center=.$indirizzo.&zoom=8&size=640x200&markers=color:red%7Clabel:S%7C" . $lat . "," . $lng . "&sensor=true";
    }

    /*     * ***********************Controllo formato file ******************************* */

    public function controllaFormato($nomefile) {
        $formati_immagine = array(".jpg", ".gif", ".png", ".jpeg");
        foreach ($formati_immagine as $formato)
            if (strrpos($nomefile, $formato))
                return TRUE;
        return FALSE; // nessun formato trovato
    }

    /*     * *********************** Aggiunge Mappa ******************************* */

    public function addMap($desc, $nome) {
        if ($this->validateInput($nome) && $this->validateInput($desc)) {
            $indirizzo = $_POST['address'];
            $lat = $_POST['lats'];
            $lng = $_POST['lngs'];
            $id = mysql_result(mysql_query("SELECT MAX(id) FROM mappe"), 0);
            $id++;
            $sql = "INSERT INTO mappe ( `id`,`indirizzo`,`lat`,`lng`,`nome`,`data`,`descr`,`userId`) VALUES ('" . mysql_real_escape_string($id) . "','" . mysql_real_escape_string($indirizzo) . "','" . mysql_real_escape_string($lat) . "','" . mysql_real_escape_string($lng) . "','" . mysql_real_escape_string($nome) . "',current_timestamp(),'" . mysql_real_escape_string($desc) . "','" . mysql_real_escape_string($_SESSION['utente']->getId()) . "');";
            $query = mysql_query($sql) or trigger_error("Errore Query: " . mysql_error());
            $mappa = new Mappa($id, $_SESSION['utente']->getId(), $nome, $desc, $indirizzo, $lat, $lng, date("Y-m-d H:i:s"), null);
            $this->setNotificaMappa($id);
        }
    }

    //rimuove commento della mappa
    public function removeCommentoMappa($id, $uid) {
        $sql1 = "DELETE FROM commenti WHERE MadeBy = '" . mysql_real_escape_string($uid->getId()) . "'and id='" . mysql_real_escape_string($id) . "'";
        $query1 = mysql_query($sql1) or trigger_error("Errore Query: " . mysql_error());
    }

    /*     * *********************** Controllo tipo di file ******************************* */

    public function controllaTipo($tipo) {
        $tipi_immagine = array("image/jpeg", "image/gif", "image/png", "image/jpg");
        foreach ($tipi_immagine as $formato) {
            if ($tipo === $formato)
                return true;
        }
        return false;
    }

}

?>