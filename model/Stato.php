<?php

class Stato{
    private $id;
    private $userId;
    private $testo;
    private $data;
    private $commenti;
    function __construct($id, $userId, $testo, $data, $commenti) {
        $this->id = $id;
        $this->userId = $userId;
        $this->testo = $testo;
        $this->data = $data;
        $this->commenti = array();
        $this->commenti = $commenti;
    }

    public function getId() {
        return $this->id;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function removeCommento($id, $commenti) {
        if(!empty($commenti) ){
        foreach ($commenti as $commento) {
            if ($commento->getId() == $id) {
                unset($commenti[array_search($commento, $commenti)]);
                return array_values($commenti);
            }
        }
        return $commenti;
        }
    }

    public function setAllCommenti($commenti) {
        $this->commenti = $commenti;
    }
    
    public function getTesto() {
        return $this->testo;
    }

    public function getData() {
        return $this->data;
    }

    public function getCommenti() {
        return $this->commenti;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setUserId($userId) {
        $this->userId = $userId;
    }

    public function setTesto($testo) {
        $this->testo = $testo;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setCommenti($commenti) {
        $this->commenti[] = $commenti;
    }


}