<?php

class Mappa {

    private $id;
    private $Userid;
    private $nome;
    private $desc;
    private $indirizzo;
    private $lat;
    private $lng;
    private $data;
    private $commenti;

    function __construct($id, $Userid, $nome, $desc, $indirizzo,$lat,$lng, $data, $commenti) {
        $this->id = $id;
        $this->Userid = $Userid;
        $this->nome = $nome;
        $this->desc = $desc;
        $this->indirizzo = $indirizzo;
        $this->lat = $lat;
        $this->lng = $lng;
        $this->data = $data;
        $this->commenti = array();
        $this->commenti = $commenti;
    }

    public function getCommenti() {
        return $this->commenti;
    }

    public function setCommenti($commenti) {
        $this->commenti[] = $commenti;
    }

    public function removeComment($id, $commenti) {
        foreach ($commenti as $commento) {
            if ($commento->getId() === $id) {
                unset($commenti[array_search($commento, $commenti)]);
                return array_values($commenti);
            }
        }
        return $commenti;
    }

    public function updateCommenti($commenti) {
        $this->commenti = $commenti;
    }

    public function getUserid() {
        return $this->Userid;
    }

    public function setUserid($Userid) {
        $this->Userid = $Userid;
    }

    public function getId() {
        return $this->id;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getDesc() {
        return $this->desc;
    }

    public function getIndirizzo() {
        return $this->indirizzo;
    }
    public function getLat() {
        return $this->lat;
    }

    public function getLng() {
        return $this->lng;
    }

    public function setLat($lat) {
        $this->lat = $lat;
    }

    public function setLng($lng) {
        $this->lng = $lng;
    }

        public function getData() {
        return $this->data;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setDesc($desc) {
        $this->desc = $desc;
    }

    public function setIndirizzo($indirizzo) {
        $this->indirizzo = $indirizzo;
    }

    public function setData($data) {
        $this->data = $data;
    }

}
?>