<?php
class Notifica{
    private $id;
    private $madeby;
    private $typeId; //id dello stato o mappa

    private $typeN;// chiarimenti sul tipo di notifica
    function __construct($id, $madeby, $typeId,$typeN) {
        $this->id = $id;
        $this->madeby = $madeby;
        $this->typeId = $typeId;
        $this->typeN = $typeN;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getMadeby() {
        return $this->madeby;
    }

    public function getTypeId() {
        return $this->typeId;
    }

    public function setMadeby($madeby) {
        $this->madeby = $madeby;
    }

    public function setTypeId($typeId) {
        $this->typeId = $typeId;
    }
    
    public function getTypeN() {
        return $this->typeN;
    }

    public function setTypeN($typeN) {
        $this->typeN = $typeN;
    }

}