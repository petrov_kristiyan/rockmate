//toglie l immagine di background
function blankBackground() {
    document.body.style.backgroundImage = "none";
}
var form;

function setXMLHttpRequest() {
    var xhr = null;
    // browser standard con supporto nativo
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return xhr;
}

$(document).ready(function() {
    
    $("#placeShare").on("mousedown", function(e) {
        $("#placeShare").data("mouseDown", true);
    });

    $("#placeShare").on("mouseup", function(e) {
        $("#placeShare").data("mouseDown", false);
    });

    $('.loading').hide();

    $('#profiloSmall').click(function() {
        var elem = $('#showInfo');
        if (elem.css('display') == 'none') {
            elem.show();
        }
        else {
            elem.hide();
        }
    });
    $('#content').click(function() {
        var elem = $('#showInfo');
        if (elem.css('display') != 'none') {
            elem.hide();
        }
    });
    switch (location.href.substr(location.href.lastIndexOf("/") + 1)) {
        case 'index.php?show=mappe':
            $(".link2").css('borderLeftColor', 'rgb(36, 123, 255)');
            break;
        case'index.php?show=amici':
            $(".link3").css('borderLeftColor', 'rgb(124, 228, 124)');
            break;
        default:
            $(".link1").css('borderLeftColor', 'red');
            break;
    }
});
$(window).scroll(function(e) {
    var nav = $('nav');
    var offset = $('.testL').offset();
    var height = $('.testL').height() + 40;
    if (offset != null) {
        var top = offset.top - height;
        var bottom = offset.top + 20;
        if ($(this).scrollTop() > top) {
            $('nav').find('a').text(' ');
            $('.link1').addClass('collision');
            $('.link2').addClass('collision');
            $('.link3').addClass('collision');
            if ($(this).scrollTop() > bottom) {
                var children = $('nav').find('a');
                children.eq(0).text(' Home');
                children.eq(1).text(' Mappe');
                children.eq(2).text(' Amici');
                $('.link1').removeClass('collision');
                $('.link2').removeClass('collision');
                $('.link3').removeClass('collision');
            }
        } else {
            var children = $('nav').find('a');
            children.eq(0).text(' Home');
            children.eq(1).text(' Mappe');
            children.eq(2).text(' Amici');
            $('.link1').removeClass('collision');
            $('.link2').removeClass('collision');
            $('.link3').removeClass('collision');
            nav.removeClass("f-nav");
        }
    }
    $('nav').css('left', -$(window).scrollLeft());
});
var xhrObj = setXMLHttpRequest();

function showNotifica1() {

    var elem = $('.msgNotifica');
    if (elem.css('display') == 'none') {
        $('.triangles1').show();
        elem.show();
    }
    else {
        $('.triangles1').hide();
        elem.hide();
    }
}
function showNotifica2() {

    var elem = $('.msgNotifica2');
    if (elem.css('display') == 'none') {
        $('.triangles2').show();
        elem.show();
    }
    else {
        $('.triangles2').hide();
        elem.hide();
    }

}
function showNotifica3() {

    var elem = $('.msgNotifica3');
    if (elem.css('display') == 'none') {
        $('.triangles3').show();
        elem.show();
    }
    else {
        $('.triangles3').hide();
        elem.hide();
    }

}
function delNot(type, id) {
    
    var elem = $('.notifiche').parent();
    if (elem.css('display') == 'none') {
        elem.show();
    }
    else {
        elem.hide();
    }
    var url, whereTo;
    if (type == '0') {
        $('.triangles1').hide();
        url = "ajax/ajax.php?delNotificaStato=" + id;
        whereTo = "index.php";
    }
    else
    if (type == '1') {
        $('.triangles2').hide();
        url = "ajax/ajax.php?delNotificaMappa=" + id;
        whereTo = "index.php?show=mappe";
    }
    else {
        $('.triangles3').hide();
        url = "ajax/ajax.php?delNotificaAmici=" + id;
        whereTo = "index.php?show=amici";
    }
    var x = setXMLHttpRequest();
    x.open("GET", url, true);
    x.onreadystatechange = function() {
        var risp = x.responseText;
        if (x.readyState == 4) {
            risp.replace(/\s/g, '');
            if (risp == 0) {
                $('.notifica').eq(0).hide();
            }
            else {
                $('.notifica').eq(0).html(risp);
                getNotifiche();
            }
        }
        window.location.assign("" + whereTo);
    };
    x.send();
    
}

function getOrdASC(c, e) {
    var url = "ajax/ajax.php?ord=" + c + "&fullNome=" + $('#showFInput').val();
    $(e.target).parent().find('img').remove()
    var img = document.createElement("img");
    img.setAttribute('src', 'src/up.png');
    img.setAttribute('width', '12');
    img.style.marginLeft = '3px';
    img.setAttribute('height', '12');
    var temp = parseInt(c);
    temp++;
    $(e.target).attr("onclick", 'getOrdDESC(' + temp + ', event)');
    $(e.target).append(img);
    xhrObj.open("GET", url, true);
    xhrObj.onreadystatechange = updateFriends;
    xhrObj.send(null);
}
function eliminaMappaBig() {
    $('.big').remove();
}

function biggerMap(id) {

    var url = "ajax/ajax.php?biggerMap=" + id;

    var x = setXMLHttpRequest();
    x.open("GET", url, true);
    x.onreadystatechange = function() {
        var risp = x.responseText;
        if (x.readyState == 4 && risp.replace(/\s/g, '').length) {
            $('.big').remove();
            $('body').prepend(risp);
            $('html, body').animate({scrollTop: 0}, 0);
            $("body").find("script").each(function(i) {
                eval($(this).text());
            });

        }
    };
    x.send();
}

function getOrdDESC(c, e) {
    var url = "ajax/ajax.php?ord=" + c + "&fullNome=" + $('#showFInput').val();
    $(e.target).parent().find('img').remove()
    var img = document.createElement("img");
    img.setAttribute('src', 'src/down.png');
    img.setAttribute('width', '12');
    img.style.marginLeft = '3px';
    img.setAttribute('height', '12');
    var temp = parseInt(c);
    temp--;
    $(e.target).attr("onclick", 'getOrdASC(' + temp + ', event)');
    $(e.target).append(img);
    xhrObj.open("GET", url, true);
    xhrObj.onreadystatechange = updateFriends;
    xhrObj.send(null);
}

function updateFriends() {
    if (xhrObj.readyState == 4) {
        var risp = xhrObj.responseText;
        document.getElementById('showF').innerHTML = risp;
    }
}

function selectDate() {
    var year = $(".year:selected").val();
    var month = $(".month:selected").val();
    var date = $(".date:selected").val();
    var curryear = new Date().getYear() + 1900;
    var t = 0;
    var leap_years = '';
    var all_years = '';
    var dates = '';
    var days = 31; //valore di default
    var leapyear = true; //true perchè 1900 è bisestile
    //ciclo for per gli anni bisestili
    for (i = 1900; i <= curryear; i += 4) {
        if (year == i) {
            t++;
            leap_years += '<option class="year" value="' + year + '" selected>' + year + '</option>'; //seleziona l anno 
        } else {
            leap_years += '<option class="year" value="' + i + '">' + i + '</option>';
        }
    }
    if (t) {
        leapyear = true;
        if ((year % 100) == 0) {
            if ((year % 400) != 0) {
                leapyear = false;
            }
        }
    } else {
        leapyear = false;
    }
    if (month == "04" || month == "06" || month == "09" || month == "10") {
        days = 30;
    }
    if (month == "02") {
        if (leapyear == true) {
            days = 29;
        } else {
            days = 28;
        }
    }
    for (i = 1; i <= days; i++) {
        if (i < 10) {
            if (date == '0' + i) {
                dates += '<option class="date" value="' + date + '" selected>' + date + '</option>';
            } else {
                dates += '<option class="date" value="0' + i + '">0' + i + '</option>';
            }
        }
        else {
            if (date == i) {
                dates += '<option class="date" value="' + date + '" selected>' + date + '</option>';
            } else {
                dates += '<option class="date" value="' + i + '">' + i + '</option>';
            }
        }
    }
    $("#d").html(dates);
    if (month == "02" && date == 29) {
        $("#y").html(leap_years);
    }
    else
    {
        for (i = 1900; i <= curryear; i++) {
            if (year == i) {
                all_years += '<option class="year" value="' + year + '" selected>' + year + '</option>';
            } else {
                all_years += '<option class="year" value="' + i + '">' + i + '</option>';
            }
        }
        $("#y").html(all_years);
    }
}
function showF(fullname, e) {
    var code = e.keyCode || e.which();
    if (check(fullname) && code != 13) {
        var url = "ajax/ajax.php?showF=" + fullname;
        var x = setXMLHttpRequest();
        x.open("GET", url, true);
        x.onreadystatechange = function() {
            var risp = x.responseText;
            if (x.readyState == 4) {
                risp.replace(/\s/g, '');
                if (risp == 0) {
                    $('#ordinamento').css('display', 'none');
                }
                document.getElementById('showF').innerHTML = risp;
                var scripts = document.getElementById('showF').getElementsByTagName("script");
                for (var i = 0; i < scripts.length; ++i) {
                    var script = scripts[i];
                    eval(script.innerHTML);
                }
            }
        };
        x.send();
    }
}

function removeStato(stato) {
    var url = "ajax/ajax.php?removeStato=" + stato;
    var x = setXMLHttpRequest();
    $('.commenti[data-id="' + stato + '"]').parent().remove();
    x.open("GET", url, true);
    x.send();
}

function removeMappa(mappa, e) {
    var url = "ajax/ajax.php?removeMappa=" + mappa;
    var x = setXMLHttpRequest();
    $(e.target).parent().parent().remove();
    x.open("GET", url, true);
    x.send();
}

function removeCommento(commento, stato, e) {
    var url = "ajax/ajax.php?removeCommento=" + commento + '&removeCommentoStato=' + stato;
    var x = setXMLHttpRequest();
    x.open("GET", url, true);
    x.onreadystatechange = function() {
        var risp = x.responseText;
        if (x.readyState == 4) {
            var parente = $(e.target).parent().parent();
            parente.empty();
            parente.append(risp);

        }
    };
    x.send();
}
function removeCommentoMappa(commento, mappa, e) {
    var url = "ajax/ajax.php?removeCommentoM=" + commento + "&removeCommentoMappa=" + mappa;
    var x = setXMLHttpRequest();
    x.open("GET", url, true);
    x.onreadystatechange = function() {
        var risp = x.responseText;
        if (x.readyState == 4) {
            var parente = $(e.target).parent();
            parente.remove();
            parente.append(risp);
        }
    };
    x.send();
}
function personalAlert(title, msg, type) {
    $(document.body).append("<form id='message' method='post' action=''><h3>" + title + "</h3> <p>" + msg + "</p> <input type='button' class='alertButton' onclick='$(this).parent().remove()' value='OK' onsubmit='$(this).parent().remove()' /></form>");
    if (type == 0) {
        $('#message').css('background-color', 'rgba(66, 162, 66, 1)');
        $('#message').css('border-color', 'green');
    }
    $('#message').dialog({
        modal: true,
        open: function(event, ui) {
            // Hide close button
            $(this).parent().children().children(".ui-dialog-titlebar-close").hide();
        },
        minHeight: '0',
    });
}
function check(elem) {
    if (elem.replace(/\n|\r|\s/g, "").length != 0) {
        tests = /\+|\||\-\-|\=|\<|\>|\!\=|\(|\)|\%|\*/i;
        if (tests.test(elem)) {
            personalAlert("Errore input", "Hai inserito uno o piu caratteri non validi: |  ,  +  ,  --  ,  =  ,  <  , >  , !=  , (  , )  ,%  , * ", 1);
            return false;
        }
    }
    return true;
}

function addF(id, e) {
    var url = "ajax/ajax.php?addF=" + id;
    var x = setXMLHttpRequest();
    x.open("GET", url, true);
    x.onreadystatechange = function() {
        var risp = x.responseText;
        if (x.readyState == 4) {
            $('#listaAmici').append(risp);
            e.parentNode.parentNode.removeChild(e.parentNode);
        }
    };
    x.send();
}
function addFS(id,search, e) {
    var url = "ajax/ajax.php?addFS=" + id+"&searchD="+search;
    var x = setXMLHttpRequest();
    x.open("GET", url, true);
    x.onreadystatechange = function() {
        var risp = x.responseText;
        if (x.readyState == 4) {
            $(".search").empty();
            $(".search").append(risp);
        }
    };
    x.send();
}
function requestF(id, e) {
    var url = "ajax/ajax.php?addF=" + id;
    var x = setXMLHttpRequest();
    x.open("GET", url, true);
    x.onreadystatechange = function() {
        var risp = x.responseText;
        if (x.readyState == 4) {
            e.parentNode.parentNode.removeChild(e.parentNode);
            $('#listaFollowing').append(risp);
            personalAlert('Successo', 'Richiesta di amicizia effettuata', 0);
        }
    };
    x.send();
}

function getComments(id) {
    var url = "ajax/ajax.php?getComments=" + id;
    $('.commenti[data-id="' + id + '"] .showComments p').text("Hide Comments");
    var img = document.createElement("img");
    img.setAttribute('src', 'src/up.png');
    img.setAttribute('width', '12');
    img.setAttribute('height', '12');
    img.onclick = function() {
        hideComments(id);
    };
    $('.commenti[data-id="' + id + '"] .showComments p').append(img);
    $('.commenti[data-id="' + id + '"] .loading').show();
    xhrObj.open("GET", url, true);

    xhrObj.onreadystatechange = function() {
        updateShowComments(id);
    };
    xhrObj.send();
}

function updateShowComments(id) {
    if (xhrObj.readyState == 4) {
        var risp = xhrObj.responseText;
        form = $('.commenti[data-id="' + id + '"] .comment');
        $('.commenti[data-id="' + id + '"] .comment').remove();
        $('.commenti[data-id="' + id + '"]').append(risp);
        $('.commenti[data-id="' + id + '"]').append(form);
        $('.commenti[data-id="' + id + '"] .loading').hide();
    }
}
function hideComments(id) {
    $('.commenti[data-id="' + id + '"] .showComments p').text("Show All " + $('.commenti[data-id="' + id + '"] .showComments p').data("num") + " Comments");
    $('.commenti[data-id="' + id + '"] .showComments p img').attr('onclick', 'getComments()');
    var img = document.createElement("img");
    img.setAttribute('src', 'src/down.png');
    img.setAttribute('width', '12');
    img.setAttribute('height', '12');
    img.onclick = function() {
        getComments(id);
    };
    $('.commenti[data-id="' + id + '"] .comment').remove();
    $('.commenti[data-id="' + id + '"] .showComments p').append(img);
    $('.loading').hide();
    $('.commenti[data-id="' + id + '"]').children().slice(4).detach();
    $('.commenti[data-id="' + id + '"]').append(form);
}
function setComment(id) {
    var msg = $('.commentaStato[data-id="' + id + '"]').val();
    if (check(msg)) {
        var url = "ajax/ajax.php?setComments=" + id + "&comment=" + msg;
        xhrObj.open("GET", url, true);
        xhrObj.onreadystatechange = function() {
            if (xhrObj.readyState == 4) {
                var risp = xhrObj.responseText;
                if (risp.replace(/\s/g, '').length) {
                    $('.commentaStato[data-id="' + id + '"]').val('');
                    $('.commenti[data-id="' + id + '"] .showComments').remove();
                    $('.commenti[data-id="' + id + '"] .desc').remove();
                    $('.commenti[data-id="' + id + '"] ').prepend(risp);
                }
                else {
                    personalAlert("Errore stato", "Questo stato è stato eliminato dal proprietario", 1);
                    $('.commenti[data-id="' + id + '"] ').parent().remove();
                }
            }
        };
        xhrObj.send();
    }
}


function setCommentMappa(id) {
    var msg = $('#update').val();
    if (check(msg)) {
        var url = "ajax/ajax.php?setCommentsMappa=" + id + "&commentMappa=" + msg;
        xhrObj.open("GET", url, true);
        xhrObj.onreadystatechange = function() {
            if (xhrObj.readyState == 4) {
                var risp = xhrObj.responseText;
                if (risp.replace(/\s/g, '').length) {
                    $('#update').val('');
                    $('.mapCommenti').empty();
                    $('.mapCommenti').append(risp);
                }
                else {
                    personalAlert("Errore stato", "Questa mappa è stata eliminata dal proprietario", 1);
                    $('.big').remove();
                }
            }
        };
        xhrObj.send();
    }
}

function setStato(msg) {
    if (check(msg)) {
        var url = "ajax/ajax.php?setStato=" + msg;
        var x = setXMLHttpRequest();
        x.open("GET", url, true);
        x.onreadystatechange = function() {
            var risp = x.responseText;
            if (x.readyState == 4) {
                $('#update').val('');
                $('.pulisci').after(risp);
            }
        };
        x.send();
    }
}

function checkUpdateCommenti() {
    $(".commenti").each(function() {
        var comm = $(this);
        var id = comm.attr('data-id');
        if (comm.children('.desc').length > 0) {
            var timestamp = comm.children('.desc').attr('data-timestamp');
            var url = "ajax/ajax.php?checkUpdateCommenti=" + id + "&time=" + timestamp;
            var x = setXMLHttpRequest();
            x.open("GET", url, true);
            x.onreadystatechange = function() {
                var risp = x.responseText;
                if (x.readyState == 4 && risp.replace(/\s/g, '').length) {
                    $('.commentaStato[data-id="' + id + '"]').val('');
                    $('.commenti[data-id="' + id + '"] .showComments').remove();
                    $('.commenti[data-id="' + id + '"] .desc').remove();
                    $('.commenti[data-id="' + id + '"] ').prepend(risp);
                }
            };
            x.send();
        } else
        if (comm.children('.desc').length === 0) {
            var url = "ajax/ajax.php?newComment=" + id;
            var x = setXMLHttpRequest();
            x.open("GET", url, true);
            x.onreadystatechange = function() {
                if (x.readyState == 4) {
                    $('.loading').hide();
                    var risp = x.responseText;
                    $('.commentaStato[data-id="' + id + '"]').val('');
                    $('.commenti[data-id="' + id + '"] .showComments').remove();
                    $('.commenti[data-id="' + id + '"] .desc').remove();
                    $('.commenti[data-id="' + id + '"] ').prepend(risp);
                }
            };
            x.send();
        }
    });
    setTimeout("checkUpdateCommenti();", 10000);
}
function getNotifiche() {
    var url = "ajax/ajax.php?getNotifiche=1";
    var x = setXMLHttpRequest();
    x.open("GET", url, true);
    x.onreadystatechange = function() {
        var risp = x.responseText;
        if (x.readyState == 4) {
            $('#notifiche').empty();
            $('#notifiche').append(risp);
        }
    };
    x.send();
    setTimeout("getNotifiche();", 15000);
}
function descrizioneBig() {
    $('.descrMappa').css({
        "width": "80%",
        "height": "100px",
        "margin-top": "10px",
    });
    $('.descrMappa').attr("placeholder", "Description ( optional field )");
    $('#placeShare').css({
        "height": "107px",
        "width": "107px",
        "vertical-align": "bottom"
    });
}
function descrizioneSmall(value) {
    if ($("#placeShare").data("mouseDown") != true) {
        $('.descrMappa').css({
            "width": "auto",
            "height": "18px",
            "margin-top": "-3px"
        });
        $('.descrMappa').attr("placeholder", "Description");
        $('#placeShare').css({
            "height": "40px",
            "width": "40px",
            "vertical-align": "middle"
        });
    }
}

function checkFormMappe() {
    if ($('[name="Named"]').val().replace(/\n|\r|\s/g, "").length != 0 && $('[name="address"]').val().replace(/\n|\r|\s/g, "").length != 0) {
        tests = /\+|\||\-\-|\=|\<|\>|\!\=|\(|\)|\%|\*/i;
        if (tests.test($('[name="Named"]').val()) || tests.test($('[name="address"]').val()) || tests.test($('[name="Descrizione"]').val())) {
            personalAlert("Errore input", "Hai inserito uno o piu caratteri non validi: |  ,  +  ,  --  ,  =  ,  <  , >  , !=  , (  , )  ,%  , * ", 1);
            return false;
        }
    }
    return true;
}

function checkFormPersonalizza() {
    if ($('[name="luogo"]').val().replace(/\n|\r|\s/g, "").length != 0 && $('[name="about"]').val().replace(/\n|\r|\s/g, "").length != 0) {
        tests = /\+|\||\-\-|\=|\<|\>|\!\=|\(|\)|\%|\*/i;
        if (tests.test($('[name="luogo"]').val()) || tests.test($('[name="about"]').val())) {
            personalAlert("Errore input", "Hai inserito uno o piu caratteri non validi: |  ,  +  ,  --  ,  =  ,  <  , >  , !=  , (  , )  ,%  , * ", 1);
            return false;
        }
    }
    return true;
}


function checkUpdateStati() {
    var timestamp = $(".stati").eq(0).attr('data-timestamp');
    var url = "ajax/ajax.php?checkUpdateStati=" + timestamp;
    var x = setXMLHttpRequest();
    x.open("GET", url, true);
    x.onreadystatechange = function() {
        var risp = x.responseText;
        if (x.readyState == 4 && risp.replace(/\s/g, '').length) {
            if (typeof timestamp === "undefined") {
                $(".pulisci").eq(0).after(risp);
            }
            else {
                $(".stati").eq(0).before(risp);

            }
            eval($('#removeStato').text());
        }
    };
    x.send();
    $('#removeStato').remove;

    setTimeout("checkUpdateStati();", 15000);
}

/*
 * Mappe
 */
function checkUpdateMappe() {
    var timestamp = $(".mappe").eq(0).attr('data-timestamp');
    var url = "ajax/ajax.php?checkUpdateMappe=" + timestamp;
    var x = setXMLHttpRequest();
    x.open("GET", url, true);
    x.onreadystatechange = function() {
        var risp = x.responseText;
        if (x.readyState == 4 && risp.replace(/\s/g, '').length) {
            if (timestamp === undefined)
                $(".insertMap").eq(0).after(risp)
            else
                $(".mappe").eq(0).before(risp);
        }
    };
    x.send();
    setTimeout("checkUpdateMappe();", 15000);
}
