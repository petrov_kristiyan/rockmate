<?php
ob_start();
include("model/User.php");
include("model/Stato.php");
include("model/Mappa.php");
include("model/Commento.php");
include("model/Notifica.php");
session_start();
include('model/dbConn.php');
require("model/Model.php");
include('view/header.php');

class Controller {

    public $model;

    public function __construct() {
        $this->model = new Model();
    }

    public function invoke() {
        include('controller/headPersonalizza.php');
        //controllo l azione scelta , login register logout o personalizza
        if (isset($_POST['register'])) {
            // Inserisco i dati nel database
            if (!empty($_POST['email1']) && !empty($_POST['email2']) && !empty($_POST['password']) && !empty($_POST['nome']) && !empty($_POST['cognome'])) {
                if ($_POST['email1'] == $_POST['email2']) {
                    if ($this->model->registraUtente($_POST['email1'], $_POST['password'], $_POST['nome'], $_POST['cognome'])) {
                        //se è stato premuto il pulsante done del form di modifica dati personali allora vai alla homepage
                        include('view/headLogin.php');
                        include('view/personalizzaRegistrazione.php');
                    }
                } else
                    $this->model->phpAlert("Errore", "Le email inserite non sono uguali");
            } else {
                $this->model->phpAlert("Errore", "Non sono stati inseriti TUTTI i dati richiesti.");
            }
        } else
        if (isset($_POST['login'])) {
            if ($_POST['email'] != '' && $_POST['password'] != '') {
                if ($this->model->validaUtente($_POST['email'], $_POST['password']) === 0) {
                    $this->model->phpAlert("Errore", "Password o Email errata.");
                } else {
                    //utente normale 
                    if ($this->model->validaUtente($_POST['email'], $_POST['password']) === 1) {
                        include('view/home.php');
                        include('controller/headHome.php');
                    } else {
                        //amministratore
                        header('Location:admin/index.php');
                    }
                }
            } else {
                $this->model->phpAlert("Errore", "Email e Password sono richiesti per effettuare il login.");
            }
        } else
        if (isset($_POST['logout'])) {
            if ($this->model->loggedIn()) {
                $this->model->logoutUser();
            }
        } else {
            if ($this->model->loggedIn() && $_SESSION['loggedin'] === 1) {
                include('view/home.php');
                include('controller/headHome.php');
            }
            if ($this->model->loggedIn() && $_SESSION['loggedin'] === 2) {
                header('Location:admin/index.php');
            }
        }
// se non sono loggato
        if (!$this->model->loggedIn()) {
            include_once ('view/headLogin.php');
            $email = "";
            if (isset($_POST['email'])) {
                $email = $_POST['email'];
            }
            include('view/login.php');
        }
        include('view/footer.php');
    }

}
?>