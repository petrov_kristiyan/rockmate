<?php
//aggiunge mappa
if (isset($_POST['invia'])) {
    if (isset($_POST['Descrizione']) || isset($_POST['Named'])) {
        $this->model->addMap($_POST['Descrizione'], $_POST['Named']);
    }
}
//mostra pagina richiesta
if (isset($_GET['show'])) {
    switch (strtolower($_GET['show'])) {
        case 'amici':
            include('view/amici.php');
            break;
        case 'mappe':
            include('view/mappe.php');
            break;
        case 'personalizza':
            include('view/personalizza.php');
            break;
        case 'search':
            include('view/search.php');
            break;
        default:
            include('view/stato.php');
            break;
    }
} else
if (isset($_GET['user']))
    include('view/user.php');
else
if (isset($_GET['erro'])) {
    if (!empty($_POST['CommentaStato']))
        include('view/stato.php');
}
else
if (isset($_GET['follow']))
    $this->model->addF($_GET['follow']);
else
if (isset($_GET['add']))
    $this->model->addF($_GET['add']);
else {
    include('view/stato.php');
}
?>
