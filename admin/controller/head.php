<?php

if (isset($_GET['show'])) {
    switch (strtolower($_GET['show'])) {
        case 'users':
            include('view/utenti.php');
            break;
        default:
            include('view/utenti.php');
            break;
    }
} else
if (isset($_GET['img'])) {
    $img = $_GET['img'];
    $_SESSION['mappa'] = $this->model->getMappa($img);
    if (isset($_POST['remComments'])) {
        $this->model->removeComments($_SESSION['mappa']);
        $_SESSION['mappa']->updateCommenti(null);
    }
    if (isset($_POST['remMap'])) {
        $this->model->removeMappa($_SESSION['mappa']->getId(), $_SESSION['utente']);
        header("Location:index.php?user=" . $_SESSION['utente']->getId());
    }
    if (isset($_POST['conferma'])) {
        if (!empty($_POST['nomeM'])) {
            $this->model->setNomeM($_SESSION['mappa'], $_POST['nomeM']);
            $_SESSION['mappa']->setNome($_POST['nomeM']);
        }
        if (!empty($_POST['descrM'])) {
            $this->model->setDescrM($_SESSION['mappa'], $_POST['descrM']);
            $_SESSION['mappa']->setDesc($_POST['descrM']);
        }
    }

    include('view/mappa.php');
} else
if (!empty($_GET['rem'])) {
    $_SESSION['mappa']->updateCommenti($this->model->removeCommentoMappa($_GET['rem'], $_SESSION['utente']));
    include('view/mappa.php');
} else
if (isset($_GET['user'])) {
    $_SESSION['utente'] = $this->model->getUtente($_GET['user']);
    if (!empty($_POST['done'])) {
        if (isset($_POST['nome']))
            $this->model->setNome($_SESSION['utente'], $_POST['nome']);
        if (isset($_POST['cognome']))
            $this->model->setCognome($_SESSION['utente'], $_POST['cognome']);
        if (isset($_POST['email']))
            $this->model->setEmail($_SESSION['utente'], $_POST['email']);
        if (isset($_POST['day']) && isset($_POST['month']) && isset($_POST['year']))
            $this->model->setNascita($_SESSION['utente'], $_POST['day'], $_POST['month'], $_POST['year']);
        if (isset($_POST['luogo']))
            $this->model->setLuogo($_SESSION['utente'], $_POST['luogo']);
        if (isset($_POST['about']))
            $this->model->setAbout($_SESSION['utente'], $_POST['about']);
        if (!empty($_FILES['profilo'])) {
            $this->model->setImgProfilo();
        }
        if (!empty($_POST['PasswdN1']) && !empty($_POST['PasswdN2'])) {
            if ($_POST['PasswdN1'] !== $_POST['PasswdN2'])
                $this->model->phpAlert("Errore", "Inserite due password diverse");
            else
                $this->model->setPasswdAdmin($_POST['PasswdN1'],$_SESSION['utente']);
        } else
            $this->model->phpAlert("Errore", "Immetere in tutti e due i campi la password");
    }
    include('view/user.php');
    if (isset($_POST['remUser'])) {
        $this->model->removeUser($_SESSION['utente']);
        header("Location:index.php");
    }
    if (isset($_POST['remMaps']))
        $this->model->removeMaps($_SESSION['utente']);
    if (isset($_POST['viewMaps']))
        include('view/maps.php');
    if (isset($_POST['viewAllStatus']))
        include('view/stato.php');
} else
    include('view/utenti.php');
?>


