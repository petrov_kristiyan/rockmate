<?php
ob_start();
include("../model/User.php");
include("../model/Mappa.php");
include("../model/Commento.php");
include("../model/Stato.php");
include("../model/Notifica.php");
session_start();
include('../model/dbConn.php');
require("../model/Model.php");
$_SESSION['error'] = "";

class Controller {

    public $model;

    public function __construct() {
        $this->model = new Model();
    }
    public function invoke() {
        include('view/header.php');
         if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'logout':
                    if ($this->model->loggedIn()) {
                        $this->model->logoutUser();
                        ob_end_clean();
                        header('Location:../index.php');
                    }
                    
            }
            unset($_GET['action']);
        } else {
            if ($this->model->loggedIn()&& $_SESSION['loggedin'] === 2 ) {   
                include('view/home.php');
                include('controller/head.php');
            }
        }
       include('view/footer.php');
    }
}