<?php
include('../../model/User.php');
include('../../model/dbConn.php');
include('../../model/Model.php');
include("../../model/Mappa.php");
include("../../model/Commento.php");
include("../../model/Stato.php");
include("../../model/Notifica.php");
session_start();
$ajax = new Ajax();

if (isset($_GET['statusUp'])) {
    $ajax->ord();
}
if (isset($_GET['getComments'])) {
    $ajax->updateComment();
}

if (!empty($_GET['removeStato'])) {
    $ajax->removeStato($_GET['removeStato']);
}

if (!empty($_GET['removeCommento']) && !empty($_GET['removeCommentoStato'])) {
    $ajax->removeCommento($_GET['removeCommentoStato'], $_GET['removeCommento']);
}

class Ajax {

    public $model;

    public function __construct() {
        $this->model = new Model();
    }

        public function removeCommento($stato, $commento) {
            $lista = $this->model->removeCommentoStato($commento, $_SESSION['utente']);
            $this->newComment($stato);
        }

        public function updateComment() {
            $lista = $this->model->getCommentiStati($_GET['getComments']);
            $i = 2;
            for ($i; $i < count($lista);) {
                echo "<div class='desc' data-timestamp='" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                if ($lista[$i]->getMadeBy() === $_SESSION['utente']->getId()) {
                    echo "<img class='deleteCommento' src='../src/close2.png' alt=''onclick='removeCommento(" . $lista[$i]->getId() . "," . $_GET['getComments'] . ",event)' />";
                }
                echo"<img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/><p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . '</p><time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time><br/>' . $lista[$i]->getTesto() . '</div>';
                $i++;
            }
        }

        public function removeStato($stato) {
            $this->model->removeStato($stato, $_SESSION['utente']);
        }

        public function setComment() {
            $value = $this->model->addCommentsStato($_GET['setComments'], $_GET['comment']);
            if ($value === false) {
                return "";
            }
            $lista = $this->model->getCommentiStati($_GET['setComments']);
            $k = 0;
            if (count($lista) >= 1) {
                if (count($lista) > 2) {
                    echo "  <div class='showComments'>
                <p style='color:silver;font:normal 13px Roboto,arial,sans-serif;'data-num='" . count($lista) . "'>";

                    echo "View all " . count($lista) . " Comments ";
                    echo "<img src='../src/down.png' width='12' height='12' alt='' onclick='getComments(" . $_GET['setComments'] . ")' /></p>
            </div>";
                    $k = 2;
                } else
                if (count($lista) == 1)
                    $k = 1;
                else
                    $k = 2;
            }
            $i = 0;
            for ($i; $i < $k;) {
                echo "<div class='desc' data-timestamp = '" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                if ($lista[$i]->getMadeBy() === $_SESSION['utente']->getId()) {
                    echo "<img class='deleteCommento' src='../src/close2.png' alt=''onclick='removeCommento(" . $lista[$i]->getId() . "," . $_GET['setComments'] . ",event)' />";
                }
                echo "<a href='index.php?user=" . $lista[$i]->getId() . "'><img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/><p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . ' </p></a><time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time><br/>' . $lista[$i]->getTesto() . '</div>';
                $i++;
            }
        }

        public function newComment($stato) {
            $lista = $this->model->getCommentiStati($stato);
            $k = 0;
            if (count($lista) >= 1) {
                if (count($lista) > 2) {
                    echo "  <div class='showComments'>
                <p style='color:silver;font:normal 13px Roboto,arial,sans-serif;'data-num='" . count($lista) . "'>";

                    echo "View all " . count($lista) . " Comments ";
                    echo "<img src='../src/down.png' width='12' height='12' alt='' onclick='getComments(" . $stato . ")' /></p>
            </div>";
                    $k = 2;
                } else
                if (count($lista) == 1)
                    $k = 1;
                else
                    $k = 2;
            }
            $i = 0;
            for ($i; $i < $k;) {
                echo "<div class='desc' data-timestamp='" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                if ($lista[$i]->getMadeBy() === $_SESSION['utente']->getId()) {
                    echo "<img class='deleteCommento' src='../src/close2.png' alt=''onclick='removeCommento(" . $lista[$i]->getId() . "," . $stato . ",event)' />";
                }
                echo "<a href='index.php?user=" . $lista[$i]->getId() . "'><img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/><p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . ' </p></a><time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time><br/>' . $lista[$i]->getTesto() . '</div>';
                $i++;
            }
        }
}
