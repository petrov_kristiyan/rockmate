    <h1>Status</h1>
    <div id='status' style='margin-left:-20px;'>
    <?php
    $stati = $this->model->getStatiHome($_SESSION['utente']->getId());
    foreach ($stati as $stato) {
        $lista = $this->model->getCommentiStati($stato->getId());
        ?>

        <div class="stati" data-timestamp="<?php echo date_create($stato->getData())->getTimestamp() ?>">
            <?php
            if ($stato->getUserId() == $_SESSION['utente']->getId()) {
                echo "<img class='delete' src='../src/delete.png' alt='' onclick='removeStato(" . $stato->getId() . ")' />";
            }
            ?>
            <div class='prova'>
                <a href="index.php?user=<?php echo $stato->getUserId(); ?>">
                    <img class='imgThumb' style='float:left;margin:0 10px;' src="<?php echo $this->model->getImgProfilo($stato->getUserId()); ?>" alt=""/>
                </a>
                <div>
                    <a href="index.php?user=<?php echo $stato->getUserId(); ?>">
                        <p class='nome' style='font:bold 15px Roboto,arial,sans-serif;margin-top: 3px;'><?php echo $this->model->getNome($stato->getUserId()) . ' ' . $this->model->getCognome($stato->getUserId()); ?> : </p>
                    </a>
                    <p class='tempo'>Shared 
                        <time>
                            <?php
                            echo $this->model->showTimestamp(date_create($stato->getData()));
                            ?>
                        </time></p>
                    <div class='status'><br/><p style='font:normal 13px Roboto,arial,sans-serif;word-wrap:break-word;'> <?php echo $stato->getTesto(); ?></p>
                    </div>
                </div>
            </div>
            <div class='commenti' data-id="<?php echo $stato->getId(); ?>">
                <?php
                $k = 0;
                if (count($lista) >= 1) {
                    if (count($lista) > 2) {
                        echo "  <div class='showComments'>
                <p style='color:silver;font:normal 13px Roboto,arial,sans-serif;'data-num='" . count($lista) . "'>";

                        echo "Show All " . count($lista) . " Comments ";
                        echo "<img src='../src/down.png' width='12' height='12' alt='' onclick='getCommentsAdmin(" . $stato->getId() . ")' /></p>
            </div>";
                        $k = 2;
                    } else
                    if (count($lista) == 1)
                        $k = 1;
                    else
                        $k = 2;
                }
                $i = 0;
                for ($i; $i < $k;) {
                    echo "<div class='desc' data-timestamp='" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                    if ($lista[$i]->getMadeBy() == $_SESSION['utente']->getId()) {
                        echo "<img class='deleteCommento' src='../src/close2.png' alt='' onclick='removeCommento(" . $lista[$i]->getId() . "," . $stato->getId() . ",event)' />";
                    }
                    echo "<a href='index.php?user=" . $lista[$i]->getMadeby() . "'>"
                    . "<img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/>"
                    . "<p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . '</p></a>'
                    . '<time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time>'
                    . '<br/>' . $lista[$i]->getTesto() . ''
                    . '</div>';
                    $i++;
                }
                ?>
                <img class='loading' alt='' src='../src/load.gif' display='none'/>
            </div>
        </div>
        <?php
    }
    ?>
