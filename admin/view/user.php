<?php
$yr = date("Y");
$months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$mIntarray = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");

$day = DateTime::createFromFormat('d M Y', $_SESSION['utente']->getDataN())->format('d');
$month = DateTime::createFromFormat('d M Y', $_SESSION['utente']->getDataN())->format('m');
$year = DateTime::createFromFormat('d M Y', $_SESSION['utente']->getDataN())->format('Y');
$m = "";
$y = "";
$d = "";
for ($i = 0; $i < 12; $i++) {
    if ($month == $mIntarray[$i])
        $m .= '<option class="month" selected value="' . $mIntarray[$i] . '">' . $months[$i] . '</option>';
    else
        $m .= '<option class="month" value="' . $mIntarray[$i] . '">' . $months[$i] . '</option>';
}

for ($i = 1; $i <= 31; $i++) {
    if ($i < 10) {
        if ($day == "0" . $i)
            $d .= '<option class="date" selected value="0' . $i . '">0' . $i . '</option>';
        else
            $d .= '<option class="date"  value="0' . $i . '">0' . $i . '</option>';
    }
    else {
        if ($day == $i)
            $d .= '<option class="date" selected value="' . $i . '">' . $i . '</option>';
        else
            $d .= '<option class="date" value="' . $i . '">' . $i . '</option>';
    }
}
for ($i = 1900; $i <= $yr; $i++) {
    if ($year == $i)
        $y .= '<option class="year" selected value="' . $i . '">' . $i . '</option>';
    else
        $y .= '<option class="year" value="' . $i . '">' . $i . '</option>';
}

echo "
    <div class='forms' style='text-align:left;'>
    <form  name='personalizza' method='post' action='index.php?user=" . $_SESSION['utente']->getId() . "' enctype='multipart/form-data'  onsubmit='return checkFormPersonalizza()' autocomplete='off'>
    <figure>
        <img class='' width='100%' height='50%' src='" . $this->model->getImgProfilo($_SESSION['utente']->getId()) . "' alt='' />
    </figure>
    <div style='text-align:center;'><select name='month' id='m' onChange='selectDate()'>" . $m . "</select>
    <select name='day' id='d' onChange='selectDate()'>" . $d . "</select>
    <select name='year' id='y' onChange='selectDate()'>" . $y . "</select><br/>
    <input type='text' name='email' value='" . $_SESSION['utente']->getEmail() . "' />
    <input type='text' name='nome' value='" . $_SESSION['utente']->getNome() . "' />
    <input type='text' name='cognome' value='" . $_SESSION['utente']->getCognome() . "' />    
    <input type='text' name='luogo' value='" . $_SESSION['utente']->getLuogoN() . "' placeholder='Luogo di Nascita' />
    <input type='password' name='PasswdN1'  placeholder='Nuova Password' pattern='^[A-Za-z0-9_]{1,15}$' title='numbers or letters without special characters. ( min 6 - max 15 )'/>
    <input type='password' name='PasswdN2'  placeholder='Ripeti Password' pattern='^[A-Za-z0-9_]{1,15}$' title='numbers or letters without special characters. ( min 6 - max 15 )'/><br /><br/>
    <textarea  name='about' style='display:inline;' placeholder='About me'>" . $_SESSION['utente']->getDescrizione() . "</textarea><br /> 
    <input type='file' name='profilo' />
    <input type='submit' style='width:100px;' name='done' class='inputB' value='Done'/>
    </div>
    </form>
    </div>";
echo "<form style='' name='personalizza' method='post' action=''>
         <input type='submit' style='width:224px;'name='remUser' value='Remove User'/>
         <input type='submit' name='remMaps' value='Remove Maps anc Comments'/>
         <input type='submit' style='width:224px;'name='viewMaps' value='View all Maps' onclick='sposta()'/>
         <input type='submit' style='width:224px;'name='viewAllStatus' value='View all Status' onclick='spostaS()'/>
         </form>";
?>
     

