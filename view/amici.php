<section id="content">
    <header id="notifiche">
        <span>
            <div class="notifica" style='border-color: red;background: rgba(253, 51, 51, 0.68);' onclick="showNotifica1()"> </div>
            <div class="notifica" style='border-color: rgb(36, 123, 255);left:28px;background-color: rgba(0, 132, 255, 0.61);' onclick="showNotifica2()"></div>
            <div class="notifica" style='border-color: rgb(124, 228, 124);left:56px;background-color: rgb(124, 228, 124);' onclick="showNotifica3()"></div>
        </span>
        <div class='triangles1'></div>
        <div class='triangles2'></div>
        <div class='triangles3'></div>
        <?php
        $notificheStato = $this->model->getNotificheStato($_SESSION['utente']->getId());
        if (count($notificheStato) != 0) {
            ?><script>
                $('.notifica').eq(0).html('<?php echo count($notificheStato); ?>');
                $('.notifica').eq(0).show();
            </script>
            <?php
            echo "<div class='msgNotifica'>";
            foreach ($notificheStato as $notifica) {
                echo "<span class='notifiche' onclick='delNot(0," . $notifica->getTypeId() . ")'><b class='places' style='color:red;'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                . "</b> commented <b class='places'> " . $this->model->getNome($this->model->getStatoUserId($notifica->getTypeId())) . " " . $this->model->getCognome($this->model->getStatoUserId($notifica->getTypeId())) . "'s status</b></span>";
            }
            echo "</div>";
        }
        ?>
        <?php
        $notificheMappe = $this->model->getNotificheMappa($_SESSION['utente']->getId());
        if (count($notificheMappe) != 0) {
            ?><script>
                $('.notifica').eq(1).html(<?php echo count($notificheMappe); ?>);
                $('.notifica').eq(1).show();
            </script>
            <?php
            echo "<div class='msgNotifica2'>";
            foreach ($notificheMappe as $notifica) {
                if ($notifica->getTypeN() == '1') {
                    echo "<span class='notifiche' onclick= 'delNot(1," . $notifica->getTypeId() . ")'><b class='places' style='color:rgba(0, 132, 255, 0.61);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                    . "</b> added a new Map </span>";
                } else {
                    echo "<span class='notifiche' onclick='delNot(1," . $notifica->getTypeId() . ")'><b class='places' style='color:rgba(0, 132, 255, 0.61);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                    . "</b> commented <b class='places'> " . $this->model->getNome($this->model->getMappaUserId($notifica->getTypeId())) . " " . $this->model->getCognome($this->model->getMappaUserId($notifica->getTypeId())) . "'s Map</b></span>";
                }
            }
            echo "</div>";
        }
        $notificheAmici = $this->model->getNotificheAmici($_SESSION['utente']->getId());
        if (count($notificheAmici) != 0) {
            ?><script>
                $('.notifica').eq(2).html(<?php echo count($notificheAmici); ?>);
                $('.notifica').eq(2).show();
            </script>
            <?php
            echo "<div class='msgNotifica3'>";
            foreach ($notificheAmici as $notifica) {
                echo "<span class='notifiche' onclick= 'delNot(2," . $notifica->getId() . ")' ><b class='places' style='color:rgb(124, 228, 124);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                . "</b> added you as friend</span>";
            }
            echo "</div>";
        }
        ?>
    </header>
    <figure>
        <img src='src/add.png' width='100px' height='100px'/>
        <figcaption>   
            <input id='showFInput' type="text" style='' placeholder="Add a new Friend !!!" onkeyup="showF(this.value, event)" />
            <br/>
            <div style='display: inline-block;margin-top: 40px;'>
                <div id ='ordinamento' style='display: none;margin-left:17px;' >
                    <button type='button' class='bOrd' onclick='getOrdASC(1, event)' > Alphabetic order </button>
                    <button type='button' class='bOrd' onclick='getOrdASC(3, event)' > Birthplace </button>
                    <button type='button' class='bOrd' onclick='getOrdASC(5, event)' > Birthday </button>
                </div>
                <div id="showF">
                </div>
            </div>
        </figcaption>
    </figure>
    <div style='float:right;width: 40%;margin-top: 1%;margin-right: 60px;'>
        <div id ='listaFollowers'><h1>Followers</h1>	<?php
            $friends = $this->model->getFollowers($_SESSION['utente']->getId());
            if (!empty($friends))
                foreach ($friends as $friend) {
                    echo "<div class ='lista'><div style='float:left;'>"
                    . "<img src = '" . $this->model->getImgProfilo($friend->getId()) . "' alt='' width='100px' height='100px'/>"
                    . "</div>"
                    . "<div style='float:left;margin-left: 10px;text-align: justify;'>"
                    . "<div class='nameFriend'>" . $friend->getNome() . "  " . $friend->getCognome() . "" . "</div>"
                    . "<p>" . $friend->getDataN() . "</p>"
                    . "<p>" . $friend->getLuogoN() . "</p>"
                    . "</div>"
                    . "<input type='button' class='followAdd' value='Add as friend' onclick='addF(" . $friend->getId() . ",this)'></input>"
                    . "</div>";
                }
            ?>
        </div>
        <div id ='listaFollowing'><h1>Following</h1>	<?php
            $friends = $this->model->getFollowing($_SESSION['utente']->getId());
            if (!empty($friends))
                foreach ($friends as $friend) {
                    echo "<div class ='lista'><div style='float:left;'>"
                    . "<img src = '" . $this->model->getImgProfilo($friend->getId()) . "' alt='' width='100px' height='100px'/>"
                    . "</div>"
                    . "<div style='float:left;margin-left: 10px;text-align: justify;'>"
                    . "<div class='nameFriend'>" . $friend->getNome() . "  " . $friend->getCognome() . "" . "</div>"
                    . "<p>" . $friend->getDataN() . "</p>"
                    . "<p>" . $friend->getLuogoN() . "</p>"
                    . "</div>"
                    . "</div>";
                }
            ?>
        </div>
    </div>
    <div style='display: inline-block;width:40%;margin-top: 1%;float:right;margin-right:60px;' id ='listaAmici'><h1>Friends</h1>	<?php
        $friends = $this->model->getFriends($_SESSION['utente']->getId());
        if (!empty($friends))
            foreach ($friends as $friend) {
                echo "<div class ='lista'>"
                . "<a href='index.php?user=" . $friend->getId() . "' style='float:left;'>"
                . "<img src = '" . $this->model->getImgProfilo($friend->getId()) . "' alt='' width='100px' height='100px'/>"
                . "</a>"
                . "<div style='float:left;margin-left: 10px;text-align: justify;'>"
                . "<a href='index.php?user=" . $friend->getId() . "' class='nameFriend'>" . $friend->getNome() . "  " . $friend->getCognome() . "</a>"
                . "<p>" . $friend->getDataN() . "</p>"
                . "<p>" . $friend->getLuogoN() . "</p>"
                . "</div>"
                . "</div>";
            }
        ?>
    </div>

