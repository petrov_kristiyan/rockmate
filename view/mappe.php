<script>
    setTimeout("getNotifiche();", 15000);
</script>
<section id="content">
    <header id="notifiche">
        <span>
            <div class="notifica" style='border-color: red;background: rgba(253, 51, 51, 0.68);' onclick="showNotifica1()"> </div>
            <div class="notifica" style='border-color: rgb(36, 123, 255);left:28px;background-color: rgba(0, 132, 255, 0.61);' onclick="showNotifica2()"></div>
            <div class="notifica" style='border-color: rgb(124, 228, 124);left:56px;background-color: rgb(124, 228, 124);' onclick="showNotifica3()"></div>
        </span>
        <div class='triangles1'></div>
        <div class='triangles2'></div>
        <div class='triangles3'></div>
        <?php
        if(isset($_SESSION['goTo'])){
            echo "<script>$(document).ready(function(){ $('html,body').animate({ scrollTop: $('.mappe[data-id=\"" . $_SESSION['goTo'] . "\"]').offset().top -400 }, 'slow');});</script>";
        unset($_SESSION['goTo']);    
        }
        $notificheStato = $this->model->getNotificheStato($_SESSION['utente']->getId());
        if (count($notificheStato) != 0) {
            ?><script>
                $('.notifica').eq(0).html('<?php echo count($notificheStato); ?>');
                $('.notifica').eq(0).show();
            </script>
            <?php
            echo "<div class='msgNotifica'>";
            foreach ($notificheStato as $notifica) {
                echo "<span class='notifiche' onclick='delNot(0," . $notifica->getTypeId() . ")'><b class='places' style='color:red;'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                . "</b> commented <b class='places'> " . $this->model->getNome($this->model->getStatoUserId($notifica->getTypeId())) . " " . $this->model->getCognome($this->model->getStatoUserId($notifica->getTypeId())) . "'s status </b></span>";
            }
            echo "</div>";
        }
        ?>
        <?php
        $notificheMappe = $this->model->getNotificheMappa($_SESSION['utente']->getId());
        if (count($notificheMappe) != 0) {
            ?><script>
                $('.notifica').eq(1).html(<?php echo count($notificheMappe); ?>);
                $('.notifica').eq(1).show();
            </script>
            <?php
            echo "<div class='msgNotifica2'>";
            foreach ($notificheMappe as $notifica) {
                if ($notifica->getTypeN() == '1') {
                    echo "<span class='notifiche' onclick= 'delNot(1," . $notifica->getTypeId() . ")'><b class='places' style='color:rgba(0, 132, 255, 0.61);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                    . "</b> added a new Map </span>";
                } else {
                    echo "<span class='notifiche' onclick='delNot(1," . $notifica->getTypeId() . ")'><b class='places' style='color:rgba(0, 132, 255, 0.61);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                    . "</b> commented <b class='places'> " . $this->model->getNome($this->model->getMappaUserId($notifica->getTypeId())) . " " . $this->model->getCognome($this->model->getMappaUserId($notifica->getTypeId())) . "'s Map</b></span>";
                }
            }
            echo "</div>";
        }
        $notificheAmici = $this->model->getNotificheAmici($_SESSION['utente']->getId());
        if (count($notificheAmici) != 0) {
            ?><script>
                $('.notifica').eq(2).html(<?php echo count($notificheAmici); ?>);
                $('.notifica').eq(2).show();
            </script>
            <?php
            echo "<div class='msgNotifica3'>";
            foreach ($notificheAmici as $notifica) {
                echo "<span class='notifiche' onclick= 'delNot( 2," . $notifica->getId() . ")' ><b class='places' style='color:rgb(124, 228, 124);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                . "</b> added you as friend</span>";
            }
            echo "</div>";
        }
        ?>
    </header>
    <script>
        setTimeout("checkUpdateMappe();", 15000);
        var geocoder;
        var map;
        var lat;
        var lng;
        function initialize() {
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(45.070982, 7.685676);
            var mapOptions = {
                zoom: 8,
                center: latlng
            }
            lat = "45.07098200000001";
            lng = "07.68567600000058";
            document.getElementById("lat").value = lat;
            document.getElementById("lng").value = lng;
            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        }

        function codeAddress() {
            var address = document.getElementById('address').value;
            if (address != '') {
                geocoder.geocode({'address': address}, function(results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        lat = results[0].geometry.location.lat();
                        lng = results[0].geometry.location.lng();
                        map.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: map,
                            position: results[0].geometry.location
                        });
                        document.getElementById("lat").value = lat;
                        document.getElementById("lng").value = lng;
                    } else {
                        personalAlert('Geocode error: ' + status);
                    }
                });
            }


        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <form method="post" action="" enctype="multipart/form-data" onsubmit='return checkFormMappe()' class ="insertMap">
        <div class='triangle'></div>
        <div id="map-canvas"></div>
        <div style="text-align: left;width: 100%;background: whitesmoke;" >     
            <input id="address"  required  placeholder="Address" name="address" onblur="codeAddress()" type="textbox" >
            <input type= "text" required placeholder="Name"  name="Named" />
            <span  data-tip="This field is not required" >
                <textarea name ="Descrizione" class="descrMappa" placeholder="Description" onfocus="descrizioneBig()" onblur="descrizioneSmall()"></textarea>
            </span>
            <input type="submit" name="invia" title="Share" value="" id="placeShare"/>
        </div>
        <input id="lat" name="lats" type="hidden" >
        <input id="lng" name="lngs" type="hidden">
    </form>

    <?php
    $mappe = $this->model->getMappeHome($_SESSION['utente']->getId());
    for ($i = 0; $i < count($mappe); $i++) {
        echo "<div class='mappe' data-id='" . $mappe[$i]->getId() . "' data-timestamp='" . date_create($mappe[$i]->getData())->getTimestamp() . "'>";
        echo "<span style='position:absolute;top:-35px;width:640px;'><a href='index.php?user=" . $mappe[$i]->getUserid() . "' style='left: 0;position: absolute;top: -2px;' ><img class='imgThumb' style='' src=" . $this->model->getImgProfilo($mappe[$i]->getUserid()) . " alt=''/></a>";
        if ($mappe[$i]->getUserid() == $_SESSION['utente']->getId()) {
            echo "<img class='delete' src='src/delete.png' alt='' onclick='removeMappa(" . $mappe[$i]->getId() . ",event)' />";
        }
        echo "<div style='margin-left:55px;text-align:left;'><a href='index.php?user=" . $mappe[$i]->getUserid() . "'><p class='nome' style='font:bold 15px Roboto,arial,sans-serif;margin-top: -5px;text-align:left;color:rgb(36, 123, 255);'>" . $this->model->getNome($mappe[$i]->getUserid()) . " " . $this->model->getCognome($mappe[$i]->getUserid()) . " </p></a>
                    <p class='tempo' style='text-align:left;margin-top:-17px;'>Added
                        <time>
                           " . $this->model->showTimestamp(date_create($mappe[$i]->getData())) . "
                        </time></p></div></span>"
        . "<img src = '" . $this->model->getMapThumb($mappe[$i]->getIndirizzo(), $mappe[$i]->getLat(), $mappe[$i]->getLng()) . "' alt='' style='border-radius:3px;' onclick=\"biggerMap('" . $mappe[$i]->getId() . "');\" class='smallMap'/></div>";
    }
    ?>
    <br/>

