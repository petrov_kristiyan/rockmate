<!DOCTYPE html>
<html style="width:100%;height:100%;position:relative;">
    <head>
        <title>Rock Mate</title>
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=yes">
        <meta name="HandheldFriendly" content="True" />
        <meta charset="utf-8" />
        <link rel="stylesheet"  href="css/style.css"/>
        <link rel="shortcut icon" href="favicon.ico"/>   
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true"></script>
        <script src="jquery-1.11.0.min.js"></script>
        <script src="script.js" ></script> 
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    </head>
