<section id="content">
    <header id="notifiche">
        <span>
            <div class="notifica" style='border-color: red;background: rgba(253, 51, 51, 0.68);' onclick="showNotifica1()"> </div>
            <div class="notifica" style='border-color: rgb(36, 123, 255);left:28px;background-color: rgba(0, 132, 255, 0.61);' onclick="showNotifica2()"></div>
            <div class="notifica" style='border-color: rgb(124, 228, 124);left:56px;background-color: rgb(124, 228, 124);' onclick="showNotifica3()"></div>
        </span>
        <div class='triangles1'></div>
        <div class='triangles2'></div>
        <div class='triangles3'></div>
        <?php
        $notificheStato = $this->model->getNotificheStato($_SESSION['utente']->getId());
        if (count($notificheStato) != 0) {
            ?><script>
                $('.notifica').eq(0).html('<?php echo count($notificheStato); ?>');
                $('.notifica').eq(0).show();
            </script>
            <?php
            echo "<div class='msgNotifica'>";
            foreach ($notificheStato as $notifica) {
                echo "<span class='notifiche' onclick='delNot(0," . $notifica->getTypeId() . ")'><b class='places' style='color:red;'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                . "</b> commented <b class='places'> " . $this->model->getNome($this->model->getStatoUserId($notifica->getTypeId())) . " " . $this->model->getCognome($this->model->getStatoUserId($notifica->getTypeId())) . "'s status</b></span>";
            }
            echo "</div>";
        }
        ?>
        <?php
        $notificheMappe = $this->model->getNotificheMappa($_SESSION['utente']->getId());
        if (count($notificheMappe) != 0) {
            ?><script>
                $('.notifica').eq(1).html(<?php echo count($notificheMappe); ?>);
                $('.notifica').eq(1).show();
            </script>
            <?php
            echo "<div class='msgNotifica2'>";
            foreach ($notificheMappe as $notifica) {
                if ($notifica->getTypeN() == '1') {
                    echo "<span class='notifiche' onclick= 'delNot(1," . $notifica->getTypeId() . ")'><b class='places' style='color:rgba(0, 132, 255, 0.61);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                    . "</b> added a new Map </span>";
                } else {
                    echo "<span class='notifiche' onclick='delNot(1," . $notifica->getTypeId() . ")'><b class='places' style='color:rgba(0, 132, 255, 0.61);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                    . "</b> commented <b class='places'> " . $this->model->getNome($this->model->getMappaUserId($notifica->getTypeId())) . " " . $this->model->getCognome($this->model->getMappaUserId($notifica->getTypeId())) . "'s Map</b></span>";
                }
            }
            echo "</div>";
        }
        $notificheAmici = $this->model->getNotificheAmici($_SESSION['utente']->getId());
        if (count($notificheAmici) != 0) {
            ?><script>
                $('.notifica').eq(2).html(<?php echo count($notificheAmici); ?>);
                $('.notifica').eq(2).show();
            </script>
            <?php
            echo "<div class='msgNotifica3'>";
            foreach ($notificheAmici as $notifica) {
                echo "<span class='notifiche' onclick= 'delNot( 2," . $notifica->getId() . ")' ><b class='places' style='color:rgb(124, 228, 124);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                . "</b> added you as friend</span>";
            }
            echo "</div>";
        }
        ?>
    </header>
    <?php
    if (!empty($_POST['searchData']) && $this->model->validateInput($_POST['searchData'])) {
        $utenti = $this->model->searchNome($_POST['searchData']);
        $amici = $this->model->getFriends($_SESSION['utente']->getId());
        $following = $this->model->getFollowing($_SESSION['utente']->getId());
        $utentiNati = $this->model->searchLuogoNascita($_POST['searchData']);
        $mappeAmici = $this->model->searchLuogoMappe($_POST['searchData']);
        $utentiMappeLuoghi = $this->model->searchLuogoMappeNA($_POST['searchData']);

        echo "<div class='search'>";
        if (!empty($utenti) && !empty($_POST['searchData'])) {
            echo "<h1>Users  : " . $_POST['searchData'] . "</h1>";
            foreach ($utenti as $item) {
                echo "<div class ='lista' style='float:none;display:inline-block; width:50%;'>"
                . ""
                . "<img style ='float:left;' src = '" . $this->model->getImgProfilo($item->getId()) . "' alt='' width='100px' height='100px'/>"
                . ""
                . "<div style='float:left;margin-left: 10px;text-align: justify;'>"
                . "<p class='nameFriend'>" . $item->getNome() . "  " . $item->getCognome() . "</p>"
                . "<p>" . $item->getDataN() . "</p>"
                . "<p>" . $item->getLuogoN() . "</p>"
                . "</div>";
                if (!(in_array($item, $amici)) && !(in_array($item, $following))) {
                    echo "\n<input type='button' class='followAdd' value='Add as friend' onclick='addFS(" . $item->getId() . ",\"" . $_POST['searchData'] . "\",this)'></input>"
                    . "</div>";
                } else {
                    echo "</div>";
                }
            }
        }
        if (!empty($utentiNati) && !empty($_POST['searchData'])) {
            echo "<h1>Users born in " . $_POST['searchData'] . "</h1>";
            foreach ($utenti as $item) {
                echo "<div class ='lista' style='float:none;display:inline-block; width:50%;'>"
                . ""
                . "<img style ='float:left;' src = '" . $this->model->getImgProfilo($item->getId()) . "' alt='' width='100px' height='100px'/>"
                . ""
                . "<div style='float:left;margin-left: 10px;text-align: justify;'>"
                . "<p class='nameFriend'>" . $item->getNome() . "  " . $item->getCognome() . "</p>"
                . "<p>" . $item->getDataN() . "</p>"
                . "<p>" . $item->getLuogoN() . "</p>"
                . "</div>";
                if (!(in_array($item, $amici)) && !(in_array($item, $following))) {
                    echo "<input type='button' class='followAdd' value='Add as friend' onclick='addFS(" . $item->getId() . ",\"" . $_POST['searchData'] . "\",this)'></input>"
                    . "</div>";
                } else {
                    echo "</div>";
                }
            }
        }
        if (!empty($utentiMappeLuoghi) && !empty($_POST['searchData'])) {
            echo "<h1>Users that have visited " . $_POST['searchData'] . "</h1>";
            foreach ($utentiMappeLuoghi as $item) {
                echo "<div class ='lista' style='float:none;display:inline-block; width:50%;'>"
                . ""
                . "<img style ='float:left;' src = '" . $this->model->getImgProfilo($item->getId()) . "' alt='' width='100px' height='100px'/>"
                . ""
                . "<div style='float:left;margin-left: 10px;text-align: justify;'>"
                . "<p class='nameFriend'>" . $item->getNome() . "  " . $item->getCognome() . "</p>"
                . "<p>" . $item->getDataN() . "</p>"
                . "<p>" . $item->getLuogoN() . "</p>"
                . "</div>";
                if (!(in_array($item, $amici)) && !(in_array($item, $following))) {
                    echo "<input type='button' class='followAdd' value='Add as friend' onclick='addFS(" . $item->getId() . ",\"" . $_POST['searchData'] . "\",this)'></input>"
                    . "</div>";
                } else {
                    echo "</div>";
                }
            }
        }
        if (!empty($mappeAmici) && !empty($_POST['searchData'])) {
            echo "<h1>Friends that have visited " . $_POST['searchData'] . "</h1>";
            foreach ($mappeAmici as $item) {
                echo "<div class='mappe' data-id='" . $item->getId() . "' data-timestamp='" . date_create($item->getData())->getTimestamp() . "'>";
                echo "<span style='position:absolute;top:-35px;width:640px;'><a href='index.php?user=" . $item->getUserid() . "' style='left: 0;position: absolute;top: -2px;' ><img class='imgThumb' style='' src=" . $this->model->getImgProfilo($item->getUserid()) . " alt=''/></a>";
                if ($item->getUserid() == $_SESSION['utente']->getId()) {
                    echo "<img class='delete' src='src/delete.png' alt='' onclick='removeMappa(" . $item->getId() . ",event)' />";
                }
                echo "<div style='margin-left:55px;text-align:left;'><a href='index.php?user=" . $item->getUserid() . "'><p class='nome' style='font:bold 15px Roboto,arial,sans-serif;margin-top: -5px;text-align:left;color:rgb(36, 123, 255);'>" . $this->model->getNome($item->getUserid()) . " " . $this->model->getCognome($item->getUserid()) . " </p></a>
                    <p class='tempo' style='text-align:left;margin-top:-17px;'>Added
                        <time>
                           " . $this->model->showTimestamp(date_create($item->getData())) . "
                        </time></p></div></span>"
                . "<img src = '" . $this->model->getMapThumb($item->getIndirizzo(), $item->getLat(), $item->getLng()) . "' alt='' style='border-radius:3px;' onclick=\"biggerMap('" . $item->getId() . "');\" class='smallMap'/></div>";
            }
        }
        if (empty($utenti) && empty($utentiNati) && empty($utentiMappeLuoghi) && empty($mappeAmici)) {
            echo "<p>No results </p>";
        }
        echo "</div>";
    }
 else {
      echo "<p>No results </p>";
    }