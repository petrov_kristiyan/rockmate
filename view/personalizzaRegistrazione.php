</header> 
<?php
    $yr = date("Y");
    $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    $mIntarray = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
    $day = "01";
    $month = "01";
    $year = "1980";
    $m = "";
    $y = "";
    $d = "";
    for ($i = 0; $i < 12; $i++) {
        if ($month == $mIntarray[$i])
            $m .= '<option class="month" selected value="' . $mIntarray[$i] . '">' . $months[$i] . '</option>';
        else
            $m .= '<option class="month" value="' . $mIntarray[$i] . '">' . $months[$i] . '</option>';
    }
    
    for ($i = 1; $i <= 31; $i++) {
        if ($i < 10) {
            if ($day == "0" . $i)
                $d .= '<option class="date" selected value="0' . $i . '">0' . $i . '</option>';
            else
                $d .= '<option class="date"  value="0' . $i . '">0' . $i . '</option>';
        }
        else {
            if ($day == $i)
                $d .= '<option class="date" selected value="' . $i . '">' . $i . '</option>';
            else
                $d .= '<option class="date" value="' . $i . '">' . $i . '</option>';
        }
    }
    for ($i = 1900; $i <= $yr; $i++) {
        if ($year == $i)
            $y .= '<option class="year" selected value="' . $i . '">' . $i . '</option>';
        else
            $y .= '<option class="year" value="' . $i . '">' . $i . '</option>';
    }

    echo "
    <div class='forms' style='text-align:left;width:36%;'>
    <form  name='personalizza' method='post' action='' enctype='multipart/form-data' onsubmit='return checkFormPersonalizza()' autocomplete='off'>
    <figure>
        <img class='' width='100%' height='50%' src='" . $this->model->getImgProfilo($_SESSION['utente']->getId()) . "' alt='' />
    </figure>
    <select name='month' id='m' onChange='selectDate()'>" . $m . "</select>
    <select name='day' id='d' onChange='selectDate()'>" . $d . "</select>
    <select name='year' id='y' onChange='selectDate()'>" . $y . "</select><br/>
    <input type='text' name='luogo' value='" . $_SESSION['utente']->getLuogoN() . "' placeholder='Birthplace' /><br />
    <textarea  name='about' placeholder='About me' >" . $_SESSION['utente']->getDescrizione() . "</textarea><br /> 
    <input type='file' name='profilo'/>
    <input type='submit' name='skip' class='inputG' value='SKIP'/>    
    <input type='submit' name='done' class='inputB' value='Done'/>
    </form>
    </div>";
    /*
     */
    ?>
