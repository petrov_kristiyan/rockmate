<div id="content" >
    <header id="notifiche">
        <span>
            <div class="notifica" style='border-color: red;background: rgba(253, 51, 51, 0.68);' onclick="showNotifica1()"> </div>
            <div class="notifica" style='border-color: rgb(36, 123, 255);left:28px;background-color: rgba(0, 132, 255, 0.61);' onclick="showNotifica2()"></div>
            <div class="notifica" style='border-color: rgb(124, 228, 124);left:56px;background-color: rgb(124, 228, 124);' onclick="showNotifica3()"></div>
        </span>
        <div class='triangles1'></div>
        <div class='triangles2'></div>
        <div class='triangles3'></div>
        <?php
        $notificheStato = $this->model->getNotificheStato($_SESSION['utente']->getId());
        if (count($notificheStato) != 0) {
            ?><script>
                $('.notifica').eq(0).html('<?php echo count($notificheStato); ?>');
                $('.notifica').eq(0).show();
            </script>
            <?php
            echo "<div class='msgNotifica'>";
            foreach ($notificheStato as $notifica) {
                echo "<span class='notifiche' onclick='delNot(0," . $notifica->getTypeId() . ")'><b class='places' style='color:red;'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                . "</b> commented <b class='places'> " . $this->model->getNome($this->model->getStatoUserId($notifica->getTypeId())) . " " . $this->model->getCognome($this->model->getStatoUserId($notifica->getTypeId())) . "'s status</b></span>";
            }
            echo "</div>";
        }
        ?>
        <?php
        $notificheMappe = $this->model->getNotificheMappa($_SESSION['utente']->getId());
        if (count($notificheMappe) != 0) {
            ?><script>
                $('.notifica').eq(1).html(<?php echo count($notificheMappe); ?>);
                $('.notifica').eq(1).show();
            </script>
            <?php
            echo "<div class='msgNotifica2'>";
            foreach ($notificheMappe as $notifica) {
                if ($notifica->getTypeN() == '1') {
                    echo "<span class='notifiche' onclick= 'delNot(1," . $notifica->getTypeId() . ")'><b class='places' style='color:rgba(0, 132, 255, 0.61);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                    . "</b> added a new Map </span>";
                } else {
                    echo "<span class='notifiche' onclick='delNot(1," . $notifica->getTypeId() . ")'><b class='places' style='color:rgba(0, 132, 255, 0.61);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                    . "</b> commented <b class='places'> " . $this->model->getNome($this->model->getMappaUserId($notifica->getTypeId())) . " " . $this->model->getCognome($this->model->getMappaUserId($notifica->getTypeId())) . "'s Map</b></span>";
                }
            }
            echo "</div>";
        }
        $notificheAmici = $this->model->getNotificheAmici($_SESSION['utente']->getId());
        if (count($notificheAmici) != 0) {
            ?><script>
                $('.notifica').eq(2).html(<?php echo count($notificheAmici); ?>);
                $('.notifica').eq(2).show();
            </script>
            <?php
            echo "<div class='msgNotifica3'>";
            foreach ($notificheAmici as $notifica) {
                echo "<span class='notifiche' onclick= 'delNot( 2," . $notifica->getId() . ")' ><b class='places' style='color:rgb(124, 228, 124);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                . "</b> added you as friend</span>";
            }
            echo "</div>";
        }
        ?>
    </header>
    <?php
    $idF = $_GET['user'];
    $friends = $this->model->getFriends($_SESSION['utente']->getId());
    $friend;
    $numF = -1;
    for ($i = 0; $i < count($friends); $i++) {
        if ($friends[$i]->getId() == $idF)
            $numF = $i;
    }
    if ($numF == -1) {
        include('profilo.php');
    } else {
        $counte = count($friends) - 1;
        echo " <figure style='min-width: 550px;margin: 0 auto;width: 60%;background: whitesmoke;border-radius: 4px;box-shadow: 1px 1px 9px;'>
                <div style='display: inline-block;'> <h1>" . $friends[$numF]->getNome() . "  " . $friends[$numF]->getCognome() . " </h1>
                     <img  class='p2' style='max-height:271px;width: 250px;vertical-align: middle;' src='" . $this->model->getImgProfilo($idF) . "' alt='' />
                 </div>
                 <div style='width: 50%;display: inline-block;vertical-align: middle;'>
                     <p style='text-align: justify;margin: 20px 10px;'>" . $friends[$numF]->getDescrizione() . "</p>
                 </div>
                  <div style='padding: 10px;'>
                     <div style='display: inline-block;padding: 10px;box-shadow: 1px 1px 8px black;margin: 10px 0;background: white;'>
                     <b> Birthday : </b> " . $friends[$numF]->getDataN() . "
                     </div> 
                    <div style='display: inline-block;padding: 10px;box-shadow: 1px 1px 8px black;background: white;'>
                      <b> Birthplace : </b>  " . $friends[$numF]->getLuogoN() . "
                     </div>
                  </div>  
             </figure>
                     <ul class='ulr'>";
        $friend = $friends[$numF];
        if (count($friends) > 1) {
            if ($friends[(($numF + 1) % count($friends))] != $friends[(($numF + $counte) % count($friends))]) {

                echo" <li class='testL'>
				<a  href='index.php?user=" . $friends[(($numF + $counte ) % count($friends))]->getId() . "'>"
                . "<h1 class='p1'>" . $friends[(($numF + $counte) % count($friends))]->getNome() . "  " . $friends[(($numF + $counte ) % count($friends))]->getCognome() . "</h1>
				<img class='photo' src='" . $this->model->getImgProfilo($friends[(($numF + $counte ) % count($friends))]->getId()) . "'  alt= ''/></a></li>";

                echo "<li class='testR'>								"
                . "<a href='index.php?user=" . $friends[(($numF + 1) % count($friends))]->getId() . "'>
                        <h1 class='p1'>" . $friends[(($numF + 1) % count($friends))]->getNome() . "  " . $friends[(($numF + 1) % count($friends))]->getCognome() . "</h1>
                                                <img class='photo' src='" . $this->model->getImgProfilo($friends[(($numF + 1) % count($friends))]->getId()) . "'  alt= ''/>
                                        </a>
                                </li>";
            } else {
                if ($numF != 1) {
                    echo "
							
					 <li class='test' style='visibility:hidden;'/>
					 <li class='testL'>
							<h1 class='p1'>" . $friends[(($numF + 1) % count($friends))]->getNome() . "  " . $friends[(($numF + 1) % count($friends))]->getCognome() . "</h1>
									<a class='photo' href='index.php?user=" . $friends[(($numF + 1) % count($friends))]->getId() . "'>
										<img class='photo' src='" . $this->model->getImgProfilo($friends[(($numF + 1) % count($friends))]->getId()) . "'  alt= ''/>
									</a>
					</li> ";
                } else {
                    echo "
					 <li class='testR'>
							<h1 class='p1'>" . $friends[(($numF + 1) % count($friends))]->getNome() . "  " . $friends[(($numF + 1) % count($friends))]->getCognome() . "</h1>
									<a class='photo' href='index.php?user=" . $friends[(($numF + 1) % count($friends))]->getId() . "'>
										<img class='photo' src='" . $this->model->getImgProfilo($friends[(($numF + 1) % count($friends))]->getId()) . "'  alt= ''/>
									</a>
					</li>
					<li class='test' style='visibility:hidden;'/>";
                }
            }
        }
        echo "</ul>";
        $stati = $this->model->getStati($idF);
        $mappe = $this->model->getMappe($idF);
        $dati = array();
        $dati = array_merge($stati, $mappe);
        $arr = array();
        $datiTime = array();
        if (!empty($dati)) {
            foreach ($dati as $dat) {
                $datiTime[] = $dat->getData();
            }
            array_multisort($datiTime, SORT_DESC, $dati);
            foreach ($dati as $dat) {
                if (is_a($dat, 'Stato')) {
                    $stato = $dat;
                    $lista = $this->model->getCommentiStati($stato->getId());
                    ?>

                    <div class="stati" data-timestamp="<?php echo date_create($stato->getData())->getTimestamp() ?>">
                        <div class='prova'>
                            <a href="index.php?user=<?php echo $stato->getUserId(); ?>">
                                <img class='imgThumb' style='float:left;padding:0 10px;' src="<?php echo $this->model->getImgProfilo($stato->getUserId()); ?>" alt=""/>
                            </a>
                            <div>
                                <p style='font:bold 15px Roboto,arial,sans-serif;margin-top: 3px;'><?php echo "<a href='index.php?user=" . $friends[$numF]->getId() . "'>" . $this->model->getNome($stato->getUserId()) . ' ' . $this->model->getCognome($stato->getUserId()) . "<a href='index.php?user=" . $friends[$numF]->getId() . "'>"; ?> : </p>
                                <p class='tempo'>Shared 
                                    <time>
                                        <?php
                                        echo $this->model->showTimestamp(date_create($stato->getData()));
                                        ?>
                                    </time></p>
                                <div class='status'><br/><p style='font:normal 13px Roboto,arial,sans-serif;word-wrap:break-word;'> <?php echo $stato->getTesto(); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class='commenti' data-id="<?php echo $stato->getId(); ?>">
                            <?php
                            $k = 0;
                            if (count($lista) >= 1) {
                                if (count($lista) > 2) {
                                    echo "  <div class='showComments'>
                <p style='color:silver;font:normal 13px Roboto,arial,sans-serif;'data-num='" . count($lista) . "'>";

                                    echo "Show All " . count($lista) . " Comments ";
                                    echo "<img src='src/down.png' width='12' height='12' alt='' onclick='getComments(" . $stato->getId() . ")' /></p>
            </div>";
                                    $k = 2;
                                } else
                                if (count($lista) == 1)
                                    $k = 1;
                                else
                                    $k = 2;
                            }
                            $i = 0;
                            for ($i; $i < $k;) {
                                echo "<div class='desc' data-timestamp='" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                                if ($lista[$i]->getMadeBy() == $_SESSION['utente']->getId()) {
                                    echo "<img class='deleteCommento' src='src/close2.png' alt='' onclick='removeCommento(" . $lista[$i]->getId() . "," . $stato->getId() . ",event)' />";
                                }
                                echo "<a href='index.php?user=" . $lista[$i]->getMadeby() . "'><img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/></a><a href='index.php?user=" . $lista[$i]->getMadeby() . "'><p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . '</p></a><time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time><br/>' . $lista[$i]->getTesto() . '</div>';
                                $i++;
                            }
                            ?>
                            <img class='loading' alt='' src='src/load.gif' display='none'/>
                            <div class='comment'  style='white-space:nowrap;margin:1%;padding:1%;' >
                                <textarea class='commentaStato' data-id='<?php echo $stato->getId(); ?>' name='CommentaStato' placeholder='Add comment...' onkeypress="{
                                                            if (event.keyCode == 13) {
                                                                event.preventDefault();
                                                                setComment(<?php echo $stato->getId(); ?>)
                                                            }
                                                        }" type='text' ></textarea>
                                <input type='button' class='send' value='' onclick="setComment(<?php echo $stato->getId(); ?>)"/>
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    $map = $dat;
                    echo "<div class='mappe'  data-id='".$map->getId()."' data-timestamp='" . date_create($map->getData())->getTimestamp() . "'><span style='position:absolute;top:-35px;'><a href='index.php?user=" . $map->getUserid() . "' style='left: 0px;position: absolute;top: -2px;'><img class='imgThumb' style='' src=" . $this->model->getImgProfilo($map->getUserid()) . " alt=''/></a>"
                    . "<div style='margin-left:55px;'><a href='index.php?user=" . $map->getUserid() . "'><p style='font:bold 15px Roboto,arial,sans-serif;margin-top: -5px;text-align:left;color:rgb(36, 123, 255);'>" . $this->model->getNome($map->getUserid()) . " " . $this->model->getCognome($map->getUserid()) . " </p></a>
                    <p class='tempo' style='text-align:left;color:black;margin-top:-12px;'>Added
                        <time>
                           " . $this->model->showTimestamp(date_create($map->getData())) . "
                        </time></p></div></span>"
                    . "<img src = '" . $this->model->getMapThumb($map->getIndirizzo(), $map->getLat(), $map->getLng()) . "' alt='' style='border-radius:10px;width:670px;' onclick=\"biggerMap('" . $map->getId() . "');\" /></div>";
                }
            }
        }
    }
    ?>
