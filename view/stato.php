<script>
    setTimeout("checkUpdateCommenti();", 10000);
    setTimeout("checkUpdateStati();", 15000);
    setTimeout("getNotifiche();", 15000);
</script>
<section id="content">
    <header id="notifiche">
        <span>
            <div class="notifica" style='border-color: red;background: rgba(253, 51, 51, 0.68);' onclick="showNotifica1()"> </div>
            <div class="notifica" style='border-color: rgb(36, 123, 255);left:28px;background-color: rgba(0, 132, 255, 0.61);' onclick="showNotifica2()"></div>
            <div class="notifica" style='border-color: rgb(124, 228, 124);left:56px;background-color: rgb(124, 228, 124);' onclick="showNotifica3()"></div>
        </span>
        <div class='triangles1'></div>
        <div class='triangles2'></div>
        <div class='triangles3'></div>
        <?php
        if(isset($_SESSION['goTo'])){
            echo "<script>$(document).ready(function(){ $('html,body').animate({ scrollTop: $('.commenti[data-id=\"" . $_SESSION['goTo'] . "\"]').offset().top -400 }, 'slow');});</script>";
            unset($_SESSION['goTo']);
            
        }
        $notificheStato = $this->model->getNotificheStato($_SESSION['utente']->getId());
        if (count($notificheStato) != 0) {
            ?><script>
                $('.notifica').eq(0).html('<?php echo count($notificheStato); ?>');
                $('.notifica').eq(0).show();
            </script>
            <?php
            echo "<div class='msgNotifica'>";
            foreach ($notificheStato as $notifica) {
                echo "<span class='notifiche' onclick='delNot(0," . $notifica->getTypeId() . ")'><b class='places' style='color:red;'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                . "</b> commented <b class='places'> " . $this->model->getNome($this->model->getStatoUserId($notifica->getTypeId())) . " " . $this->model->getCognome($this->model->getStatoUserId($notifica->getTypeId())) . "'s status</b></span>";
            }
            echo "</div>";
        }
        ?>
        <?php
        $notificheMappe = $this->model->getNotificheMappa($_SESSION['utente']->getId());
        if (count($notificheMappe) != 0) {
            ?><script>
                $('.notifica').eq(1).html(<?php echo count($notificheMappe); ?>);
                $('.notifica').eq(1).show();
            </script>
            <?php
            echo "<div class='msgNotifica2'>";
            foreach ($notificheMappe as $notifica) {
                if ($notifica->getTypeN() == '1') {
                    echo "<span class='notifiche' onclick= 'delNot(1," . $notifica->getTypeId() . ")'><b class='places' style='color:rgba(0, 132, 255, 0.61);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                    . "</b> added a new Map </span>";
                } else {
                    echo "<span class='notifiche' onclick='delNot(1," . $notifica->getTypeId() . ")'><b class='places' style='color:rgba(0, 132, 255, 0.61);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                    . "</b> commented <b class='places'> " . $this->model->getNome($this->model->getMappaUserId($notifica->getTypeId())) . " " . $this->model->getCognome($this->model->getMappaUserId($notifica->getTypeId())) . "'s Map</b></span>";
                }
            }
            echo "</div>";
        }
        $notificheAmici = $this->model->getNotificheAmici($_SESSION['utente']->getId());
        if (count($notificheAmici) != 0) {
            ?><script>
                $('.notifica').eq(2).html(<?php echo count($notificheAmici); ?>);
                $('.notifica').eq(2).show();
            </script>
            <?php
            echo "<div class='msgNotifica3'>";
            foreach ($notificheAmici as $notifica) {
                echo "<span class='notifiche' onclick= 'delNot( 2," . $notifica->getId() . ")' ><b class='places' style='color:rgb(124, 228, 124);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                . "</b> added you as friend</span>";
            }
            echo "</div>";
        }
        ?>
    </header>
    <?php
    echo "<div class='pulisci'>
                <div class='triangle'></div>
        <textarea id='update' name='updates' placeholder='What are you thinking about?' onkeypress='{
                            if (event.keyCode == 13) {
                                event.preventDefault();
                                setStato(this.value)
                            }
                        }'></textarea>
        <input type='submit'  id='share'  title='Share' value='' onclick='setStato($(\"#update\").val())'/>
       </div>";
    $stati = $this->model->getStatiHome($_SESSION['utente']->getId());
    foreach ($stati as $stato) {
        $lista = $this->model->getCommentiStati($stato->getId());
        ?>

        <div class="stati" data-timestamp="<?php echo date_create($stato->getData())->getTimestamp() ?>">
            <?php
            if ($stato->getUserId() == $_SESSION['utente']->getId()) {
                echo "<img class='delete' src='src/delete.png' alt='' onclick='removeStato(" . $stato->getId() . ")' />";
            }
            ?>
            <div class='prova'>
                <a href="index.php?user=<?php echo $stato->getUserId(); ?>">
                    <img class='imgThumb' style='float:left;margin:0 10px;' src="<?php echo $this->model->getImgProfilo($stato->getUserId()); ?>" alt=""/>
                </a>
                <div>
                    <a href="index.php?user=<?php echo $stato->getUserId(); ?>">
                        <p class='nome' style='font:bold 15px Roboto,arial,sans-serif;margin-top: 3px;'><?php echo $this->model->getNome($stato->getUserId()) . ' ' . $this->model->getCognome($stato->getUserId()); ?> : </p>
                    </a>
                    <p class='tempo'>Shared 
                        <time>
                            <?php
                            echo $this->model->showTimestamp(date_create($stato->getData()));
                            ?>
                        </time></p>
                    <div class='status'><br/><p style='font:normal 13px Roboto,arial,sans-serif;word-wrap:break-word;'> <?php echo $stato->getTesto(); ?></p>
                    </div>
                </div>
            </div>
            <div class='commenti' data-id="<?php echo $stato->getId(); ?>">
                <?php
                $k = 0;
                if (count($lista) >= 1) {
                    if (count($lista) > 2) {
                        echo "  <div class='showComments'>
                <p style='color:silver;font:normal 13px Roboto,arial,sans-serif;'data-num='" . count($lista) . "'>";

                        echo "Show All " . count($lista) . " Comments ";
                        echo "<img src='src/down.png' width='12' height='12' alt='' onclick='getComments(" . $stato->getId() . ")' /></p>
            </div>";
                        $k = 2;
                    } else
                    if (count($lista) == 1)
                        $k = 1;
                    else
                        $k = 2;
                }
                $i = 0;
                for ($i; $i < $k;) {
                    echo "<div class='desc' data-timestamp='" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                    if ($lista[$i]->getMadeBy() == $_SESSION['utente']->getId()) {
                        echo "<img class='deleteCommento' src='src/close2.png' alt='' onclick='removeCommento(" . $lista[$i]->getId() . "," . $stato->getId() . ",event)' />";
                    }
                    echo "<a href='index.php?user=" . $lista[$i]->getMadeby() . "'>"
                    . "<img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/>"
                    . "<p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . '</p></a>'
                    . '<time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time>'
                    . '<br/>' . $lista[$i]->getTesto() . ''
                    . '</div>';
                    $i++;
                }
                ?>
                <img class='loading' alt='' src='src/load.gif' display='none'/>
                <div class='comment'  style='white-space:nowrap;margin:1%;padding:1%;' >
                    <textarea class='commentaStato' data-id='<?php echo $stato->getId(); ?>' name='CommentaStato' placeholder='Add comment...' onkeypress="{
                                    if (event.keyCode == 13) {
                                        event.preventDefault();
                                        setComment(<?php echo $stato->getId(); ?>)
                                    }
                                }" type='text'></textarea>
                    <input type='button' class='send' value='' onclick="setComment(<?php echo $stato->getId(); ?>)"/>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
