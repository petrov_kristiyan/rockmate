<?php
include('../model/User.php');
include('../model/dbConn.php');
include('../model/Model.php');
include("../model/Mappa.php");
include("../model/Commento.php");
include("../model/Stato.php");
include("../model/Notifica.php");
session_start();
$ajax = new Ajax();
if (isset($_GET['ord']) && isset($_GET['fullNome'])) {
    $ajax->ord();
}
if (isset($_GET['statusUp'])) {
    $ajax->ord();
}
if (isset($_GET['getComments'])) {
    $ajax->updateComment();
}
if (isset($_GET['biggerMap'])) {
    $ajax->getBiggerMap($_GET['biggerMap']);
}
if (isset($_GET['newComment'])) {
    $ajax->newComment($_GET['newComment']);
}
if (!empty($_GET['setComments']) && !empty($_GET['comment'])) {
    $ajax->setComment();
}
if (!empty($_GET['setCommentsMappa']) && !empty($_GET['commentMappa'])) {
    $ajax->setCommentMappa();
}
if (!empty($_GET['checkUpdateCommenti'])) {
    if ($_GET['time'] != 'undefined') {
        $ajax->checkUpdateCommenti();
    }
}
if (!empty($_GET['showF'])) {
    $ajax->showFriend($_GET['showF']);
}

if (!empty($_GET['removeStato'])) {
    $ajax->removeStato($_GET['removeStato']);
}

if (!empty($_GET['removeMappa'])) {
    $ajax->removeMappa($_GET['removeMappa']);
}

if (!empty($_GET['removeCommento']) && !empty($_GET['removeCommentoStato'])) {
    $ajax->removeCommento($_GET['removeCommentoStato'], $_GET['removeCommento']);
}

if (!empty($_GET['removeCommentoMappa']) && !empty($_GET['removeCommentoM'])) {
    $ajax->removeCommentoMappa($_GET['removeCommentoMappa'], $_GET['removeCommentoM']);
}

if (!empty($_GET['addF'])) {
    $ajax->addFriend($_GET['addF']);
}
if (!empty($_GET['addFS']) && !empty($_GET['searchD'])) {
    $ajax->addFriendS($_GET['addFS'], $_GET['searchD']);
}
if (!empty($_GET['checkUpdateStati'])) {
    $ajax->checkUpdateStati();
}
if (!empty($_GET['checkUpdateMappe'])) {
    $ajax->checkUpdateMappe();
}
if (!empty($_GET['setStato'])) {
    $ajax->setStato();
}
if (!empty($_GET['getNotifiche']))
    $ajax->getNotifiche();
if (!empty($_GET['delNotificaStato']))
    $ajax->delNotificaStato($_GET['delNotificaStato']);
if (!empty($_GET['delNotificaMappa']))
    $ajax->delNotificaMappa($_GET['delNotificaMappa']);
if (!empty($_GET['delNotificaAmici']))
    $ajax->delNotificaAmici($_GET['delNotificaAmici']);

class Ajax {

    public $model;

    public function __construct() {
        $this->model = new Model();
    }

    public function ord() {
        $arrayF = $this->model->ordina($_GET['ord'], $_GET['fullNome']);
        if (!empty($arrayF)) {
            foreach ($arrayF as $friends)
                echo "<div class ='lista'><a href='index.php?user=" . $friends->getId() . "' style='float:left;'><img src = '" . $this->model->getImgProfilo($friends->getId()) . "' alt='' width='100px' height='100px'/></a><div style='float:left;margin-left: 10px;text-align: justify;'><a href='index.php?user=" . $friends->getId() . "' class='nameFriend'>" . $friends->getNome() . "  " . $friends->getCognome() . "</a><p>" . $friends->getDataN() . "</p><p>" . $friends->getLuogoN() . "</p></div><input type='button' class='followAdd' value='Add as friend' onclick='requestF(" . $friends->getId() . ",this)'></input></div>";
        }
    }

    public function showFriend($fullName) {
        $arrayF = $this->model->showF($fullName);
        if (!empty($arrayF)) {
            ?><script>$('#ordinamento').css('display', 'block');
                            $('.bOrd').find('img').remove();</script><?php
            foreach ($arrayF as $friends)
                echo "<div class ='lista'><a href='index.php?user=" . $friends->getId() . "' style='float:left;'><img src = '" . $this->model->getImgProfilo($friends->getId()) . "' alt='' width='100px' height='100px'/></a><div style='float:left;margin-left: 10px;text-align: justify;'><a href='index.php?user=" . $friends->getId() . "' class='nameFriend'>" . $friends->getNome() . "  " . $friends->getCognome() . "</a><p>" . $friends->getDataN() . "</p><p>" . $friends->getLuogoN() . "</p></div><input type='button' class='followAdd' value='Add as friend' onclick='requestF(" . $friends->getId() . ",this)'></input></div>";
        } else {
            ?><script>$('#ordinamento').css('display', 'none');</script><?php
                echo "Nessun utente trovato.";
            }
        }

        public function removeCommento($stato, $commento) {
            $lista = $this->model->removeCommentoStato($commento, $_SESSION['utente']);
            $this->newComment($stato);
            echo "<img class='loading' alt='' src='src/load.gif' display='none'/>
                    <div class='comment'  style='white-space:nowrap;margin:1%;padding:1%;' >
                        <textarea class='commentaStato' data-id='" . $stato . "' name='CommentaStato' placeholder='Aggiungi un commento...' onkeypress='{
                                            if (event.keyCode == 13) {
                                                event.preventDefault();
                                                setComment(" . $stato . ")
                                            }
                                        }' type='text'onblur='return check(this.value);' ></textarea>
                        <input type='button' class='send' value='' onclick='setComment(" . $stato . ")'/>
                    </div>";
        }

        public function removeCommentoMappa($mappa, $commento) {
            $lista = $this->model->removeCommentoMappa($commento, $_SESSION['utente']);
            $this->newCommentMappa($mappa);
        }

        public function addFriend($id) {
            $friend = $this->model->addF($id);
            echo "<div class ='lista'><a href='index.php?user=" . $friend->getId() . "' style='float:left;'><img src = '" . $this->model->getImgProfilo($friend->getId()) . "' alt='' width='100px' height='100px'/></a><div style='float:left;margin-left: 10px;text-align: justify;'><a href='index.php?user=" . $friend->getId() . "' class='nameFriend'>" . $friend->getNome() . "  " . $friend->getCognome() . "</a><p>" . $friend->getDataN() . "</p><p>" . $friend->getLuogoN() . "</p></div></div>";
        }

        public function addFriendS($id, $searchD) {
            $this->model->addF($id);
            $utenti = $this->model->searchNome($searchD);
            $amici = $this->model->getFriends($_SESSION['utente']->getId());
            $following = $this->model->getFollowing($_SESSION['utente']->getId());
            $utentiNati = $this->model->searchLuogoNascita($searchD);
            $mappeAmici = $this->model->searchLuogoMappe($searchD);
            $utentiMappeLuoghi = $this->model->searchLuogoMappeNA($searchD);
            echo "<div class='search'>";
            if (!empty($utenti)) {
                echo "<h1>Utenti  : " . $searchD . "</h1>";
                foreach ($utenti as $item) {
                    echo "<div class ='lista' style='float:none;display:inline-block; width:50%;'>"
                    . ""
                    . "<img style ='float:left;' src = '" . $this->model->getImgProfilo($item->getId()) . "' alt='' width='100px' height='100px'/>"
                    . ""
                    . "<div style='float:left;margin-left: 10px;text-align: justify;'>"
                    . "<p class='nameFriend'>" . $item->getNome() . "  " . $item->getCognome() . "</p>"
                    . "<p>" . $item->getDataN() . "</p>"
                    . "<p>" . $item->getLuogoN() . "</p>"
                    . "</div>";
                    if (!(in_array($item, $amici)) && !(in_array($item, $following))) {
                        echo "<input type='button' class='followAdd' value='Add as friend' onclick='addFS(" . $item->getId() . ",\"" . $searchD . "\",this)'></input>"
                        . "</div>";
                    } else {
                        echo "</div>";
                    }
                }
            }
            if (!empty($utentiNati)) {
                echo "<h1>Utenti nati a " . $searchD . "</h1>";
                foreach ($utenti as $item) {
                    echo "<div class ='lista' style='float:none;display:inline-block; width:50%;'>"
                    . ""
                    . "<img style ='float:left;' src = '" . $this->model->getImgProfilo($item->getId()) . "' alt='' width='100px' height='100px'/>"
                    . ""
                    . "<div style='float:left;margin-left: 10px;text-align: justify;'>"
                    . "<p class='nameFriend'>" . $item->getNome() . "  " . $item->getCognome() . "</p>"
                    . "<p>" . $item->getDataN() . "</p>"
                    . "<p>" . $item->getLuogoN() . "</p>"
                    . "</div>";
                    if (!(in_array($item, $amici)) && !(in_array($item, $following))) {
                        echo "<input type='button' class='followAdd' value='Add as friend' onclick='addFS(" . $item->getId() . ",\"" . $searchD . "\",this)'></input>"
                        . "</div>";
                    } else {
                        echo "</div>";
                    }
                }
            }
            if (!empty($utentiMappeLuoghi)) {
                echo "<h1>Utenti che sono stati a " . $searchD . "</h1>";
                foreach ($utentiMappeLuoghi as $item) {
                    echo "<div class ='lista' style='float:none;display:inline-block; width:50%;'>"
                    . ""
                    . "<img style ='float:left;' src = '" . $this->model->getImgProfilo($item->getId()) . "' alt='' width='100px' height='100px'/>"
                    . ""
                    . "<div style='float:left;margin-left: 10px;text-align: justify;'>"
                    . "<p class='nameFriend'>" . $item->getNome() . "  " . $item->getCognome() . "</p>"
                    . "<p>" . $item->getDataN() . "</p>"
                    . "<p>" . $item->getLuogoN() . "</p>"
                    . "</div>";
                    if (!(in_array($item, $amici)) && !(in_array($item, $following))) {
                        echo "<input type='button' class='followAdd' value='Add as friend' onclick='addFS(" . $item->getId() . ",\"" . $searchD . "\",this)'></input>"
                        . "</div>";
                    } else {
                        echo "</div>";
                    }
                }
            }
            if (!empty($mappeAmici)) {
                echo "<h1>Amici che hanno visitato " . $searchD . "</h1>";
                foreach ($mappeAmici as $item) {
                    echo "<div class='mappe' data-id='" . $item->getId() . "' data-timestamp='" . date_create($item->getData())->getTimestamp() . "'>";
                    echo "<span style='position:absolute;top:-35px;width:640px;'><a href='index.php?user=" . $item->getUserid() . "' style='left: 0;position: absolute;top: -2px;' ><img class='imgThumb' style='' src=" . $this->model->getImgProfilo($item->getUserid()) . " alt=''/></a>";
                    if ($item->getUserid() == $_SESSION['utente']->getId()) {
                        echo "<img class='delete' src='src/delete.png' alt='' onclick='removeMappa(" . $item->getId() . ",event)' />";
                    }
                    echo "<div style='margin-left:55px;text-align:left;'><a href='index.php?user=" . $item->getUserid() . "'><p class='nome' style='font:bold 15px Roboto,arial,sans-serif;margin-top: -5px;text-align:left;color:rgb(36, 123, 255);'>" . $this->model->getNome($item->getUserid()) . " " . $this->model->getCognome($item->getUserid()) . " </p></a>
                    <p class='tempo' style='text-align:left;margin-top:-17px;'>Added
                        <time>
                           " . $this->model->showTimestamp(date_create($item->getData())) . "
                        </time></p></div></span>"
                    . "<img src = '" . $this->model->getMapThumb($item->getIndirizzo(), $item->getLat(), $item->getLng()) . "' alt='' style='border-radius:3px;' onclick=\"biggerMap('" . $item->getId() . "');\" class='smallMap'/></div>";
                }
            } if (count($utenti) == 0 && count($utentiNati) == 0 && count($utentiMappeLuoghi) && count($mappeAmici) == 0) {
                echo "<p>No results </p>";
            }
            echo "</div>";
        }

        public function updateComment() {
            $lista = $this->model->getCommentiStati($_GET['getComments']);
            $i = 2;
            for ($i; $i < count($lista);) {
                echo "<div class='desc' data-timestamp='" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                if ($lista[$i]->getMadeBy() === $_SESSION['utente']->getId()) {
                    echo "<img class='deleteCommento' src='src/close2.png' alt=''onclick='removeCommento(" . $lista[$i]->getId() . "," . $_GET['getComments'] . ",event)' />";
                }
                echo"<img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/><p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . '</p><time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time><br/>' . $lista[$i]->getTesto() . '</div>';
                $i++;
            }
        }

        public function removeStato($stato) {
            $this->model->removeStato($stato, $_SESSION['utente']);
        }

        public function removeMappa($mappa) {
            $this->model->removeMappa($mappa, $_SESSION['utente']);
        }

        public function setComment() {
            $value = $this->model->addCommentsStato($_GET['setComments'], $_GET['comment']);
            if ($value === false) {
                return "";
            }
            $lista = $this->model->getCommentiStati($_GET['setComments']);
            $k = 0;
            if (count($lista) >= 1) {
                if (count($lista) > 2) {
                    echo "  <div class='showComments'>
                <p style='color:silver;font:normal 13px Roboto,arial,sans-serif;'data-num='" . count($lista) . "'>";

                    echo "View all " . count($lista) . " Comments ";
                    echo "<img src='src/down.png' width='12' height='12' alt='' onclick='getComments(" . $_GET['setComments'] . ")' /></p>
            </div>";
                    $k = 2;
                } else
                if (count($lista) == 1)
                    $k = 1;
                else
                    $k = 2;
            }
            $i = 0;
            for ($i; $i < $k;) {
                echo "<div class='desc' data-timestamp = '" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                if ($lista[$i]->getMadeBy() === $_SESSION['utente']->getId()) {
                    echo "<img class='deleteCommento' src='src/close2.png' alt=''onclick='removeCommento(" . $lista[$i]->getId() . "," . $_GET['setComments'] . ",event)' />";
                }
                echo "<a href='index.php?user=" . $lista[$i]->getId() . "'><img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/><p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . ' </p></a><time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time><br/>' . $lista[$i]->getTesto() . '</div>';
                $i++;
            }
        }

        public function setCommentMappa() {
            $value = $this->model->addCommentsMappa($_GET['setCommentsMappa'], $_GET['commentMappa']);
            if ($value === false) {
                return "";
            }

            $lista = $this->model->getCommentiMappa($_GET['setCommentsMappa']);
            if (!empty($lista)) {
                $i = 0;
                for ($i; $i < count($lista);) {
                    echo "<div class='desc' style='background:#f1f1f1;color:black;padding:3%;' data-timestamp='" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                    if ($lista[$i]->getMadeBy() == $_SESSION['utente']->getId()) {
                        echo "<img class='deleteCommento' src='src/close2.png' alt='' onclick='removeCommentoMappa(" . $lista[$i]->getId() . "," . $_GET['setCommentsMappa'] . ",event)' />";
                    }
                    echo "<a href='index.php?user=" . $lista[$i]->getMadeby() . "' style='color:black;'><img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/><p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . '</p></a><time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time><br/>' . $lista[$i]->getTesto() . '</div>';
                    $i++;
                }
                echo "</div>";
            }
        }

        public function delNotificaStato($stato) {
            $_SESSION['goTo'] = $stato;
            $this->model->delNotificaStato($stato);
            echo count($this->model->getNotificheStato($stato));
        }

        public function delNotificaMappa($mappa) {
            $_SESSION['goTo'] = $mappa;
            $this->model->delNotificaMappa($mappa);
            echo count($this->model->getNotificheMappa($mappa));
        }

        public function delNotificaAmici($amici) {
            $this->model->delNotificaAmici($amici);
            echo count($this->model->getNotificheAmici($amici));
        }

        public function checkUpdateCommenti() {
            $lista = $this->model->checkUpdateComments($_GET['checkUpdateCommenti'], $_GET['time']);
            $i = 0;
            if (count($lista) === 2) {
                $num = count($this->model->getCommentiStati($_GET['checkUpdateCommenti']));
                if ($num > 2) {
                    echo "  <div class='showComments'>
                <p style='color:silver;font:normal 13px Roboto,arial,sans-serif;'data-num='" . $num . "'>";

                    echo "View all " . $num . " Comments ";
                    echo "<img src='src/down.png' width='12' height='12' alt='' onclick='getComments(" . $_GET['checkUpdateCommenti'] . ")' /></p>
            </div>";
                }
                for ($i; $i < count($lista);) {
                    echo "<div class='desc' data-timestamp='" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                    if ($lista[$i]->getMadeBy() === $_SESSION['utente']->getId()) {
                        echo "<img class='deleteCommento' src='src/close2.png' alt=''onclick='removeCommento(" . $lista[$i]->getId() . "," . $_GET['checkUpdateCommenti'] . ",event)' />";
                    }
                    echo "<a href='index.php?user=" . $lista[$i]->getId() . "'><img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/><p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . '</p></a><time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time><br/>' . $lista[$i]->getTesto() . '</div>';
                    $i++;
                }
            }
        }

        public function newComment($stato) {
            $lista = $this->model->getCommentiStati($stato);
            $k = 0;
            if (count($lista) >= 1) {
                if (count($lista) > 2) {
                    echo "  <div class='showComments'>
                <p style='color:silver;font:normal 13px Roboto,arial,sans-serif;'data-num='" . count($lista) . "'>";

                    echo "View all " . count($lista) . " Comments ";
                    echo "<img src='src/down.png' width='12' height='12' alt='' onclick='getComments(" . $stato . ")' /></p>
            </div>";
                    $k = 2;
                } else
                if (count($lista) == 1)
                    $k = 1;
                else
                    $k = 2;
            }
            $i = 0;
            for ($i; $i < $k;) {
                echo "<div class='desc' data-timestamp='" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                if ($lista[$i]->getMadeBy() === $_SESSION['utente']->getId()) {
                    echo "<img class='deleteCommento' src='src/close2.png' alt=''onclick='removeCommento(" . $lista[$i]->getId() . "," . $stato . ",event)' />";
                }
                echo "<a href='index.php?user=" . $lista[$i]->getId() . "'><img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/><p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . ' </p></a><time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time><br/>' . $lista[$i]->getTesto() . '</div>';
                $i++;
            }
        }

        public function newCommentMappa($mappa) {
            $lista = $this->model->getCommentiMappa($mappa);
            $listaTime = array();
            if (!empty($lista)) {
                $i = 0;
                for ($i; $i < count($lista);) {
                    echo "<div class='desc' style='background:#f1f1f1;color:black;padding:3%;' data-timestamp='" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                    if ($lista[$i]->getMadeBy() == $_SESSION['utente']->getId()) {
                        echo "<img class='deleteCommento' src='src/close2.png' alt='' onclick='removeCommentoMappa(" . $lista[$i]->getId() . "," . $mappa->getId() . ",event)' />";
                    }
                    echo "<a href='index.php?user=" . $lista[$i]->getMadeby() . "' style='color:black;'><img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/><p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . '</p></a><time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time><br/>' . $lista[$i]->getTesto() . '</div>';
                    $i++;
                }
                echo "</div></div></aside></section>";
            }
        }

        public function getNotifiche() {
            ?> <span>
            <div class="notifica" style='border-color: red;background: rgba(253, 51, 51, 0.68);' onclick="showNotifica1()"> </div>
            <div class="notifica" style='border-color: rgb(36, 123, 255);left:28px;background-color: rgba(0, 132, 255, 0.68);' onclick="showNotifica2()"></div>
            <div class="notifica" style='border-color: rgb(124, 228, 124);left:56px;background-color: rgb(124, 228, 124);' onclick="showNotifica3()"></div>
        </span>
        <div class='triangles1'></div>
        <div class='triangles2'></div>
        <div class='triangles3'></div>
        <?php
        if (isset($_SESSION['goTo'])) {
            $gotoType = $_SESSION['goTo'][0];
            $gotoId = $_SESSION['goTo'][1];
            echo "<script>              
            if (" . $gotoType . " == '0')
                $('html, body').animate({scrollTop: ($('.commenti[data-id=" . $gotoId . "]').parent().offset().top)},0);
            if (" . $gotoType . " == '1')
                 $('html, body').animate({scrollTop: ($('.mappe[data-id=" . $gotoId . "]').parent().offset().top)},0);";
            unset($_SESSION['goTo']);
        }
        $notificheStato = $this->model->getNotificheStato($_SESSION['utente']->getId());
        if (count($notificheStato) != 0) {
            ?><script>
                            $('.notifica').eq(0).html('<?php echo count($notificheStato); ?>');
                            $('.notifica').eq(0).show();
            </script>
            <?php
            echo "<div class='msgNotifica'>";
            foreach ($notificheStato as $notifica) {
                echo "<span class='notifiche' onclick='delNot(0," . $notifica->getTypeId() . ")'><b class='places' style='color:red;'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                . "</b> ha commentato lo stato di <b class='places'> " . $this->model->getNome($this->model->getStatoUserId($notifica->getTypeId())) . " " . $this->model->getCognome($this->model->getStatoUserId($notifica->getTypeId())) . "</b></span>";
            }
            echo "</div>";
        }
        ?>
        <?php
        $notificheMappe = $this->model->getNotificheMappa($_SESSION['utente']->getId());
        if (count($notificheMappe) != 0) {
            ?><script>
                            $('.notifica').eq(1).html(<?php echo count($notificheMappe); ?>);
                            $('.notifica').eq(1).show();
            </script>
            <?php
            echo "<div class='msgNotifica2'>";
            foreach ($notificheMappe as $notifica) {
                if ($notifica->getTypeN() == '1') {
                    echo "<span class='notifiche' onclick= 'delNot(1," . $notifica->getTypeId() . ")'><b class='places' style='color:rgba(0, 132, 255, 0.61);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                    . "</b> ha inserito una nuova Mappa </span>";
                } else {
                    echo "<span class='notifiche' onclick='delNot(1," . $notifica->getTypeId() . ")'><b class='places' style='color:rgba(0, 132, 255, 0.61);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                    . "</b> ha commentato la mappa di <b class='places'> " . $this->model->getNome($this->model->getMappaUserId($notifica->getTypeId())) . " " . $this->model->getCognome($this->model->getMappaUserId($notifica->getTypeId())) . "</b></span>";
                }
            }
            echo "</div>";
        }
        $notificheAmici = $this->model->getNotificheAmici($_SESSION['utente']->getId());
        if (count($notificheAmici) != 0) {
            ?><script>
                            $('.notifica').eq(2).html(<?php echo count($notificheAmici); ?>);
                            $('.notifica').eq(2).show();
            </script>
            <?php
            echo "<div class='msgNotifica3'>";
            foreach ($notificheAmici as $notifica) {
                echo "<span class='notifiche' onclick= 'delNot( 2," . $notifica->getId() . ")' ><b class='places' style='color:rgb(124, 228, 124);'>" . $this->model->getNome($notifica->getMadeby()) . " " . $this->model->getCognome($notifica->getMadeby())
                . "</b> ti ha aggiunto come amico</span>";
            }
            echo "</div>";
        }
    }

    public function setStato() {
        $stato = $this->model->setStatus($_GET['setStato']);
        if (!empty($stato)) {
            $lista = $this->model->getCommentiStati($stato->getId());
            ?> <div class="stati" data-timestamp="<?php echo date_create($stato->getData())->getTimestamp() ?>">
            <?php
            if ($stato->getUserId() === $_SESSION['utente']->getId()) {


                echo "<img class='delete' src='src/delete.png' alt=''onclick='removeStato(" . $stato->getId() . ")' />";
            }
            ?>
                <div class='prova'>
                    <a href="index.php?user=<?php echo $stato->getUserId(); ?>">
                        <img class='imgThumb' style='float:left;padding:0 10px;' src="<?php echo $this->model->getImgProfilo($stato->getUserId()); ?>" alt=""/>
                    </a>
                    <div>
                        <p style='font:bold 15px Roboto,arial,sans-serif;margin-top: 3px;'><?php echo $this->model->getNome($stato->getUserId()) . ' ' . $this->model->getCognome($stato->getUserId()); ?> : </p>
                        <p class='tempo'>Shared 
                            <time>
                                <?php
                                echo $this->model->showTimestamp(date_create($stato->getData()));
                                ?>
                            </time></p>
                        <div class='status'><br/><p style='font:normal 13px Roboto,arial,sans-serif;word-wrap:break-word;'> <?php echo $stato->getTesto(); ?></p>
                        </div>
                    </div>
                </div>
                <div class='commenti' data-id="<?php echo $stato->getId(); ?>">
                    <?php
                    $k = 0;
                    if (count($lista) >= 1) {
                        if (count($lista) > 2) {
                            echo "  <div class='showComments'>
                <p style='color:silver;font:normal 13px Roboto,arial,sans-serif;'data-num='" . count($lista) . "'>";

                            echo "Show All " . count($lista) . " Comments ";
                            echo "<img src='src/down.png' width='12' height='12' alt='' onclick='getComments(" . $stato->getId() . ")' /></p>
            </div>";
                            $k = 2;
                        } else
                        if (count($lista) == 1)
                            $k = 1;
                        else
                            $k = 2;
                    }
                    $i = 0;
                    for ($i; $i < $k;) {
                        echo "<div class='desc' data-timestamp='" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                        if ($lista[$i]->getMadeBy() === $_SESSION['utente']->getId()) {
                            echo "<img class='deleteCommento' src='src/close2.png' alt=''onclick='removeCommento(" . $lista[$i]->getId() . "," . $stato->getId() . ",event)' />";
                        }echo "<a href='index.php?user=" . $lista[$i]->getId() . "'><img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/><p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . '</p></a><time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time><br/>' . $lista[$i]->getTesto() . '</div>';
                        $i++;
                    }
                    ?>
                    <img class='loading' alt='' src='src/load.gif' style='display:none'/>
                    <div class='comment'  style='white-space:nowrap;margin:1%;padding:1%;' >
                        <textarea class='commentaStato' data-id='<?php echo $stato->getId(); ?>' name='CommentaStato' placeholder='Aggiungi un commento...' onkeypress="{
                                    if (event.keyCode == 13) {
                                        event.preventDefault();
                                        setComment(<?php echo $stato->getId(); ?>)
                                    }
                                }" type='text' ></textarea>
                        <input type='button' class='send' value='' onclick="setComment(<?php echo $stato->getId(); ?>)"/>
                    </div>
                </div>
            </div><?php
        }
    }

    public function checkUpdateStati() {
        $time = $_GET['checkUpdateStati'];
        if ($time == 'undefined') {
            $time = date_create('1000-01-01')->getTimestamp();
        }
        $stati = $this->model->checkUpdateStati($time);
        foreach ($stati as $stato) {
            $lista = $this->model->getCommentiStati($stato->getId());
            echo '
        <div class="stati" data-timestamp="' . date_create($stato->getData())->getTimestamp() . '">
            <div class="prova">
                <a href="index.php?user=' . $stato->getUserId() . '">
                    <img class="imgThumb" style="float:left;padding:0 10px;" src="' . $this->model->getImgProfilo($stato->getUserId()) . '" alt=""/>
                </a>
                <div>
                <a href="index.php?user=' . $stato->getId() . '">
                    <p style="font:bold 15px Roboto,arial,sans-serif;margin-top: 3px;">' . $this->model->getNome($stato->getUserId()) . ' ' . $this->model->getCognome($stato->getUserId()) . ' : </p></a>
                    <p class="tempo">Shared
                        <time>
                           ' . $this->model->showTimestamp(date_create($stato->getData())) . '
                        </time></p>
                    <div class="status"><br/><p style="font:normal 13px Roboto,arial,sans-serif;word-wrap:break-word;">' . $stato->getTesto() . '</p>
                    </div>
                </div>
            </div>
            <div class="commenti" data-id="' . $stato->getId() . '">';
            $k = 0;
            if (count($lista) >= 1) {
                if (count($lista) > 2) {
                    echo "  <div class='showComments'>
                <p style='color:silver;font:normal 13px Roboto,arial,sans-serif;'data-num='" . count($lista) . "'>";

                    echo "View all " . count($lista) . " Comments ";
                    echo "<img src='src/down.png' width='12' height='12' alt='' onclick='getComments(" . $stato->getId() . ")' /></p>
            </div>";
                    $k = 2;
                } else
                if (count($lista) == 1)
                    $k = 1;
                else
                    $k = 2;
            }
            $i = 0;
            for ($i; $i < $k;) {
                echo "<div class='desc' data-timestamp='" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                if ($stato->getUserId() === $_SESSION['utente']->getId()) {
                    echo "<img class='deleteCommento' src='src/close2.png' alt=''onclick='removeCommento(" . $lista[$i]->getId() . "," . $stato->getId() . ",event)' />";
                }
                echo "<a href='index.php?user=" . $lista[$i]->getId() . "'><img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/><p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . '</p></a><time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time><br/>' . $lista[$i]->getTesto() . '</div>';
                $i++;
            }
            echo "
            <img class='loading' alt='' src='src/load.gif'/>
            <div class='comment'  style='white-space:nowrap;margin:1%;padding:1%;' >
                <textarea class='commentaStato' data-id='" . $stato->getId() . "' name='CommentaStato' placeholder='Aggiungi un commento...' onkeypress='{
                            if (event.keyCode == 13) {
                                event.preventDefault();
                                setComment(" . $stato->getId() . ")
                            }
                        }' type='text' ></textarea>
                <input type='button' class='send' value='' onclick='setComment(" . $stato->getId() . ")'/>
            </div>
        </div></div>";
        }
    }

    public function checkUpdateMappe() {
        $time = $_GET['checkUpdateMappe'];
        if ($time == 'undefined') {
            $time = date_create('1000-01-01')->getTimestamp();
        }
        $mappe = $this->model->checkUpdateMappe($time);
        for ($i = 0; $i < count($mappe); $i++) {
            echo "<div class='mappe'  data-id='" . $mappe[$i]->getId() . "' data-timestamp='" . date_create($mappe[$i]->getData())->getTimestamp() . "'><span style='position:absolute;top:-35px;'><a href='index.php?user=" . $mappe[$i]->getUserid() . "' style='left: 0;position: absolute;top: -2px;' ><img class='imgThumb' style='' src=" . $this->model->getImgProfilo($mappe[$i]->getUserid()) . " alt=''/></a>"
            . "<div style='margin-left:50px;'><a href='index.php?user=" . $mappe[$i]->getId() . "'><p style='font:bold 15px Roboto,arial,sans-serif;margin-top: -5px;text-align:left;color:rgb(36, 123, 255);'>" . $this->model->getNome($mappe[$i]->getUserid()) . " " . $this->model->getCognome($mappe[$i]->getUserid()) . " </p></a>
                    <p class='tempo' style='text-align:left;color:black;margin-top:-12px;'>Added
                        <time>
                           " . $this->model->showTimestamp(date_create($mappe[$i]->getData())) . "
                        </time></p></div></span>"
            . "<a href='index.php?img=" . $mappe[$i]->getId() . "'><img src = '" . $this->model->getMapThumb($mappe[$i]->getIndirizzo(), $mappe[$i]->getLat(), $mappe[$i]->getLng()) . "' alt='' style='border-radius:10px;'/></a></div>";
        }
    }

    public function getBiggerMap($id) {
        $mappa = $this->model->getMappa($id);

        echo "
        <div class='big' data-role='page'>"
        . "<h1 class='mapHeader' ><span><div class='nomeMap' style='position:absolute; left:26%;' >" . $mappa->getNome() . "</div>"
        . "<a href='index.php?user=" . $mappa->getUserid() . "' style='left: 2%;position: absolute;' >"
        . "<img class='imgThumb' style='background:whitesmoke;' src=" . $this->model->getImgProfilo($mappa->getUserid()) . " alt=''/></a>"
        . "<div class='userInfo'><a href='index.php?user=" . $mappa->getUserid() . "'><p class='nome' style='vertical-align:text-top;font:bold 15px Roboto,arial,sans-serif;margin-top: 0px;text-align:left;color:rgb(36, 123, 255);'>" . $this->model->getNome($mappa->getUserid()) . " " . $this->model->getCognome($mappa->getUserid()) . " </p></a>
                <p class='tempo' style='text-align:left;color:white;margin-top:-17px;'>Added
                    <time>
                        " . $this->model->showTimestamp(date_create($mappa->getData())) . "
                    </time></p></div></span><img src='src/close.png' id='closer' onclick='eliminaMappaBig()' /></h1>";
        $maps = $this->model->getMappe($mappa->getUserid());
        $numImg = 0;
        for ($ir = 0; $ir < count($maps); $ir++) {
            if ($maps[$ir]->getId() == $id) {
                $numImg = $ir;
            }
        }
        $counte = count($maps) - 1;
        echo "<section id='mapBig'><aside id='mapShow'><img src='src/prev.png' class='prev' onclick=\"biggerMap('" . $maps[(($numImg + $counte) % count($maps))]->getId() . "')\"  />";
        echo "<img src='src/next.png' class='next'  onclick=\"biggerMap('" . $maps[(($numImg + 1) % count($maps))]->getId() . "')\" />";
        echo "<div id ='ingrandisca' ></div>";
        ?>
        <script>
            var geocoder;
            var map;
            var lat =<?php echo $mappa->getLat(); ?>;
            var lng =<?php echo $mappa->getLng(); ?>;
            $('.mapCommenti').css('height', ($('#ingrandisca').outerHeight() - $('#desc').outerHeight() - $('.pulisci').outerHeight() - $('.mapDesc').outerHeight() - 50) + 'px');
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(lat, lng);
            var mapOptions = {
                zoom: 8,
                center: latlng
            }
            map = new google.maps.Map(document.getElementById('ingrandisca'), mapOptions);
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
            });
        </script>      
        <?php
        echo "</aside>
        <aside id='mapInfo' ><p class='mapDesc' >Description</p>
            <p id='desc'>" . $mappa->getDesc() . "</p>
            <form  class='pulisci' style='width:100%;margin:10px auto;' method='post' action='index.php?map=" . $id . "'>
                <div class='triangle' style='border-width:33px 30px 0px 0px;' ></div>
                <textarea id='update' style='width:82%;' name='Commenta' placeholder='Comment' onkeypress='{
                                if (event.keyCode == 13) {
                                    event.preventDefault();
                                    setCommentMappa(" . $mappa->getId() . ")
                                }
                            }'></textarea>
                <input type='button' id='share' style='min-width:13%;width:13%;background-image: url(src/Share2.png);' value='' title='Share' onclick='setCommentMappa(" . $mappa->getId() . ")' />
            </form>
            <div class='mapCommenti'>";
        $lista = array();
        $lista = $mappa->getCommenti();
        $listaTime = array();
        if (!empty($lista)) {
            $i = 0;
            for ($i; $i < count($lista);) {
                echo "<div class='desc' style='background:#f1f1f1;color:black;padding:3%;' data-timestamp='" . date_create($lista[$i]->getData())->getTimestamp() . "'>";
                if ($lista[$i]->getMadeBy() == $_SESSION['utente']->getId()) {
                    echo "<img class='deleteCommento' src='src/close2.png' alt='' onclick='removeCommentoMappa(" . $lista[$i]->getId() . "," . $mappa->getId() . ",event)' />";
                }
                echo "<a href='index.php?user=" . $lista[$i]->getMadeby() . "' style='color:black;'><img  style='border-radius:3px;height:40px;width:40px;float:left;margin-right:11px;' class='imgThumb' src='" . $this->model->getImgProfilo($lista[$i]->getMadeby()) . "'  alt=''/><p class='nome'>" . $this->model->getNome($lista[$i]->getMadeby()) . ' ' . $this->model->getCognome($lista[$i]->getMadeby()) . '</p></a><time class="timeCommenti"> Submited ' . $this->model->showTimestamp(date_create($lista[$i]->getData())) . '</time><br/>' . $lista[$i]->getTesto() . '</div>';
                $i++;
            }
            echo "</div></div></aside></section>";
        }
    }

}
